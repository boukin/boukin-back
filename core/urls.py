"""
URL configuration for core project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from api import views

router = routers.DefaultRouter()

urlpatterns = [
    path("", include(router.urls)),
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "api/swagger/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path("api/redoc/", SpectacularRedocView.as_view(url_name="schema"), name="redoc"),
    path("admin/", admin.site.urls),
    path("auth-token/", views.LoginUserView.as_view()),
    path("api-auth/", include("rest_framework.urls")),
    # User related routes:
    path("users/search/", views.GetUserView.as_view()),
    path("users/", views.CreateUserView.as_view()),
    path("users/<uuid:user_id>/", views.UpdateUserView.as_view()),
    path("users/<uuid:user_id>/loans/", views.GetAllLoansView.as_view()),
    path(
        "users/<uuid:user_id>/activation/",
        views.ActivateUserView.as_view(),
        name="user_activation",
    ),
    path("users/external/", views.CreateExternalUserView.as_view()),
    # Book related routes:
    path("books/", views.BooksView.as_view()),
    # Ownedbook related routes:
    path("owned-books/", views.OwnedBookView.as_view()),
    path("owned-books/<uuid:owned_book_id>/", views.ModifyOwnedBookView.as_view()),
    path("users/<uuid:user_id>/owned-books/", views.ListFriendOwnedBooksView.as_view()),
    path("loans/", views.CreateLoanByBookOwnerView.as_view()),
    path("loans/request/", views.RequestLoanByFriendView.as_view()),
    path("loans/<uuid:loan_id>/", views.UpdateLoanView.as_view()),
    # Friendship related routes:
    path("friendships/", views.CreateFriendshipView.as_view()),
    path("friendships/<uuid:friendship_id>/", views.ModifyFriendshipView.as_view()),
    path("users/<uuid:user_id>/friendships/", views.GetUserFriendshipsView.as_view()),
    path(
        "users/<uuid:user_id>/friendships/<uuid:friendship_id>/",
        views.GetUserSingleFriendshipView.as_view(),
    ),
]

urlpatterns += router.urls
