from rest_framework import permissions
from api.models import Friendship


class IsBookOwnerOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, book):

        if request.method in permissions.SAFE_METHODS:
            return True

        return book.owner == request.user


class IsRequestUser(permissions.BasePermission):

    def has_object_permission(self, request, view, user):

        if request.method in permissions.SAFE_METHODS:
            return True

        return user == request.user


class IsLoanOwnerOrBorrower(permissions.BasePermission):

    def has_object_permission(self, request, view, loan):

        return loan.owner == request.user or loan.borrower == request.user


class IsAFriend(permissions.BasePermission):

    def has_object_permission(self, request, view, user):

        if not Friendship.objects.filter(
            requesting_user=user,
            addressed_user=request.user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        ) and not Friendship.objects.filter(
            requesting_user=request.user,
            addressed_user=user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        ):
            return False
        return True


class IsFriendshipMember(permissions.BasePermission):

    def has_object_permission(self, request, view, friendship):

        if request.method in permissions.SAFE_METHODS:
            return True

        if (
            not request.user == friendship.addressed_user
            and not request.user == friendship.requesting_user
        ):
            return False
        return True


class IsStatusUpdateOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, friendship):

        if request.method == "PATCH":
            allowed_fields = {"status"}
            request_fields = set(request.data.keys())
            disallowed_fields = request_fields - allowed_fields
            if disallowed_fields:
                return False
        return True
