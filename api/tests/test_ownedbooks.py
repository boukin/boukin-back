from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate
from api.views import (
    OwnedBookView,
    ListFriendOwnedBooksView,
    ModifyOwnedBookView,
)
from api.models import User, Book, OwnedBook, Loan, Friendship


class ListOwnedBookTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.existing_book = Book.objects.create(
            title="titre", author_fname="John", author_lname="Doe", isbn="9781237894560"
        )
        self.owner = User.objects.create(
            username="owner", email="owner@boukin.org", password="@dminM0tdepasse"
        )
        self.available_owned_book = OwnedBook.objects.create(
            owner=self.owner,
            book=self.existing_book,
            status=OwnedBook.OwnedBookStatus.AVAILABLE,
        )
        self.loaned_owned_book = OwnedBook.objects.create(
            owner=self.owner,
            book=self.existing_book,
            status=OwnedBook.OwnedBookStatus.LOANED,
        )
        self.not_visible_owned_book = OwnedBook.objects.create(
            owner=self.owner,
            book=self.existing_book,
            status=OwnedBook.OwnedBookStatus.NOT_VISIBLE,
        )
        self.friend = User.objects.create(
            username="friend", email="friend@boukin.org", password="fri3ndM@Tdepasse"
        )
        Friendship.objects.create(
            requesting_user=self.owner,
            addressed_user=self.friend,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )

    def test_get_all_owned_books_of_request_user_with_success(self):
        request = self.factory.get("owned-books/")
        force_authenticate(request, self.owner)
        response = OwnedBookView.as_view()(request)
        returned_owned_books_ids = list()
        for owned_book in response.data:
            returned_owned_books_ids.append(owned_book["owned_book_id"])
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            str(self.available_owned_book.owned_book_id), returned_owned_books_ids
        )
        self.assertIn(
            str(self.loaned_owned_book.owned_book_id), returned_owned_books_ids
        )
        self.assertIn(
            str(self.not_visible_owned_book.owned_book_id), returned_owned_books_ids
        )

    def test_get_owned_book_with_success_returns_empty_array_if_owner_has_no_books(
        self,
    ):
        user_with_no_books = User.objects.create(
            username="no_book_user", email="nobook@boukin.org", password="P@ssw0rd"
        )
        request = self.factory.get("owned-books/")
        force_authenticate(request, user_with_no_books)
        response = OwnedBookView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [])

    def test_get_owned_books_with_error_if_request_user_not_existent(self):
        not_existent_user = User.objects.create(
            username="not_existent_user", email="nouser@boukin.org", password="P@ssw0rd"
        )
        not_existent_user.delete()
        request = self.factory.get("owned-books/")
        force_authenticate(request, not_existent_user)
        response = OwnedBookView.as_view()(request)
        self.assertEqual(response.status_code, 404)

    def test_get_available_and_loaned_owned_books_of_a_friend_with_success(self):
        owner_id = str(self.owner.user_id)
        request = self.factory.get(f"users/{owner_id}/owned-books/")
        force_authenticate(request, self.friend)
        response = ListFriendOwnedBooksView.as_view()(request, user_id=owner_id)
        returned_owned_books_ids = list()
        for owned_book in response.data:
            returned_owned_books_ids.append(owned_book["owned_book_id"])
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            str(self.available_owned_book.owned_book_id), returned_owned_books_ids
        )
        self.assertIn(
            str(self.loaned_owned_book.owned_book_id), returned_owned_books_ids
        )

    def test_get_owned_books_of_not_a_friend_fails(self):
        not_a_friend = User.objects.create(
            username="not_a_friend", email="other@boukin.org", password="P@$$W0rd"
        )
        owner_id = str(self.owner.user_id)
        request = self.factory.get(f"users/{owner_id}/owned-books/")
        force_authenticate(request, not_a_friend)
        response = ListFriendOwnedBooksView.as_view()(request, user_id=owner_id)
        self.assertEqual(response.status_code, 403)

    def test_get_owned_books_of_a_friend_does_not_return_not_visible_books(self):
        owner_id = str(self.owner.user_id)
        request = self.factory.get(f"users/{owner_id}/owned-books/")
        force_authenticate(request, self.friend)
        response = ListFriendOwnedBooksView.as_view()(request, user_id=owner_id)
        returned_owned_books_ids = list()
        for owned_book in response.data:
            returned_owned_books_ids.append(owned_book["owned_book_id"])
        self.assertNotIn(
            str(self.not_visible_owned_book.owned_book_id), returned_owned_books_ids
        )

    def test_get_owned_books_with_error_404_if_owner_not_existent(self):
        non_existent_user = User.objects.create(
            username="non_existent_user", email="user@boukin.org", password="P@$$W0rd"
        )
        owner_id = str(non_existent_user.user_id)
        non_existent_user.delete()
        request = self.factory.get(f"users/{owner_id}/owned-books/")
        force_authenticate(request, self.friend)
        response = ListFriendOwnedBooksView.as_view()(request, user_id=owner_id)
        self.assertEqual(response.status_code, 404)

    def test_get_owned_books_with_error_400_if_no_user_id_is_passed_to_the_request(
        self,
    ):
        request = self.factory.get("users/owned-books/")
        force_authenticate(request, self.friend)
        response = ListFriendOwnedBooksView.as_view()(request, user_id=None)
        self.assertEqual(response.status_code, 400)


class AddOwnedBookTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.already_existing_book = Book.objects.create(
            title="titre", author_fname="John", author_lname="Doe", isbn="9781237894560"
        )
        self.owner_user = User.objects.create(
            username="owner", email="owner@boukin.org", password="@dminM0tdepasse"
        )
        self.owned_book_data = {
            "owner": self.owner_user.user_id,
            "book": self.already_existing_book.book_id,
        }

    def test_post_owned_book_with_success(self):
        request = self.factory.post("owned-books/", self.owned_book_data)
        force_authenticate(request, self.owner_user)
        response = OwnedBookView.as_view()(request)
        self.assertEqual(response.status_code, 201)

    def test_post_owned_book_not_in_books_table_fails(self):
        deleted_book = Book.objects.create(
            title="titre", author_fname="John", author_lname="Noe", isbn="9781237894561"
        )
        deleted_book_id = deleted_book.book_id
        deleted_book.delete()
        owned_book_data = {"owner": self.owner_user.user_id, "book": deleted_book_id}
        request = self.factory.post("owned-books/", owned_book_data)
        force_authenticate(request, self.owner_user)
        response = OwnedBookView.as_view()(request)
        self.assertEqual(response.status_code, 400)

    def test_post_owned_book_from_another_owner_fails(self):
        another_user = User.objects.create(
            username="other", email="other@boukin.org", password="@dminM0tdepasse"
        )
        request = self.factory.post("owned-books/", self.owned_book_data)
        force_authenticate(request, another_user)
        response = OwnedBookView.as_view()(request)
        self.assertEqual(response.status_code, 403)

    def test_post_with_error_with_owned_book_for_non_existent_user(self):
        non_existent_user = User.objects.create(
            username="non_existent_user",
            email="nonexistentuser@boukin.org",
            password="NoN3existent_Us3R",
        )
        self.owned_book_data.update({"owner": non_existent_user.user_id})
        non_existent_user.delete()
        request = self.factory.post("owned-books/", self.owned_book_data)
        force_authenticate(request, self.owner_user)
        response = OwnedBookView.as_view()(request)
        self.assertEqual(response.status_code, 404)


class DeleteOwnedBookTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.already_existing_book = Book.objects.create(
            title="titre", author_fname="John", author_lname="Doe", isbn="9781237894560"
        )
        self.another_existing_book = Book.objects.create(
            title="titre2",
            author_fname="Jane",
            author_lname="Doe",
            isbn="9784145789147",
        )
        self.owner_user = User.objects.create(
            username="owner", email="owner@boukin.org", password="@dminM0tdepasse"
        )
        self.another_user = User.objects.create(
            username="another_user",
            email="another_user@boukin.org",
            password="@dminM0tdepasse",
        )
        self.owned_book_available = OwnedBook.objects.create(
            owner=self.owner_user,
            book=self.already_existing_book,
            status=OwnedBook.OwnedBookStatus.AVAILABLE,
        )
        self.owned_book_loaned = OwnedBook.objects.create(
            owner=self.owner_user,
            book=self.another_existing_book,
            status=OwnedBook.OwnedBookStatus.LOANED,
        )

    def test_delete_owned_book_with_success(self):
        request = self.factory.delete(
            f"owned-books/{self.owned_book_available.owned_book_id}/"
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.owned_book_available.owned_book_id
        )
        query_owned_book = OwnedBook.objects.filter(
            owned_book_id=self.owned_book_available.owned_book_id
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(query_owned_book.exists())

    def test_delete_owned_book_with_error_if_book_is_loaned(self):
        request = self.factory.delete(
            f"owned-books/{self.owned_book_loaned.owned_book_id}/"
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.owned_book_loaned.owned_book_id
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data, "Vous ne pouvez supprimer un livre en cours de prêt."
        )

    def test_delete_owned_book_with_error_if_book_does_not_exist(self):
        request = self.factory.delete("owned-books/inexistant_id/")
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(request, owned_book_id="inexistant_id")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Ce livre n'existe pas.")

    def test_delete_owned_book_with_error_if_request_user_not_the_owner(self):
        request = self.factory.delete(
            f"owned-books/{self.owned_book_available.owned_book_id}/"
        )
        force_authenticate(request, self.another_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.owned_book_available.owned_book_id
        )
        query_owned_book = OwnedBook.objects.filter(
            owned_book_id=self.owned_book_available.owned_book_id
        )
        self.assertEqual(response.status_code, 403)
        self.assertTrue(query_owned_book.exists())


class UpdateOwnedBookStatusTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.existing_book = Book.objects.create(
            title="titre", author_fname="John", author_lname="Doe", isbn="9781237894560"
        )
        self.owner_user = User.objects.create(
            username="owner", email="owner@boukin.org", password="@dminM0tdepasse"
        )
        self.available_book = OwnedBook.objects.create(
            owner=self.owner_user,
            book=self.existing_book,
            status=OwnedBook.OwnedBookStatus.AVAILABLE,
        )
        self.not_visible_book = OwnedBook.objects.create(
            owner=self.owner_user,
            book=self.existing_book,
            status=OwnedBook.OwnedBookStatus.NOT_VISIBLE,
        )
        self.loaned_book = OwnedBook.objects.create(
            owner=self.owner_user,
            book=self.existing_book,
            status=OwnedBook.OwnedBookStatus.LOANED,
        )
        self.another_user = User.objects.create(
            username="another_user",
            email="another_user@boukin.org",
            password="Another7_U53r",
        )
        self.previous_loan = Loan.objects.create(
            owner=self.owner_user,
            borrower=self.another_user,
            owned_book=self.loaned_book,
            status=Loan.LoanStatus.RETURN_CONFIRMED,
        )
        self.ongoing_loan = Loan.objects.create(
            owner=self.owner_user,
            borrower=self.another_user,
            owned_book=self.loaned_book,
            status=Loan.LoanStatus.LOAN_ACTIVE,
        )

    def test_update_ownedbook_status_from_available_to_not_visible_with_success(self):
        updated_status = OwnedBook.OwnedBookStatus.NOT_VISIBLE
        update_data = {
            "status": updated_status,
        }
        request = self.factory.patch(
            f"owned-books/{self.available_book.owned_book_id}/", update_data
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.available_book.owned_book_id
        )
        updated_book = OwnedBook.objects.get(
            owned_book_id=self.available_book.owned_book_id
        )
        resource_location = f"/owned-books/{self.available_book.owned_book_id}/"
        self.assertEqual(response.status_code, 204)
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(updated_book.status, updated_status)

    def test_update_owned_book_field_other_than_status_fails_with_error_400(self):
        other_book = Book.objects.create(
            title="titre2",
            author_fname="John",
            author_lname="Doe",
            isbn="9781237894561",
        )
        update_data = {
            "book": other_book.book_id,
        }
        request = self.factory.patch(
            f"owned-books/{self.available_book.owned_book_id}/", update_data
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.available_book.owned_book_id
        )
        not_updated_book = OwnedBook.objects.get(
            owned_book_id=self.available_book.owned_book_id
        )
        error_message = "Vous ne pouvez modifier que le statut du livre"
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))
        self.assertNotEqual(not_updated_book.book.book_id, other_book.book_id)

    def test_update_ownedbook_with_not_valid_status_fails_with_error_400(self):
        updated_status = "not_valid_status"
        update_data = {
            "status": updated_status,
        }
        request = self.factory.patch(
            f"owned-books/{self.available_book.owned_book_id}/", update_data
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.available_book.owned_book_id
        )
        not_updated_book = OwnedBook.objects.get(
            owned_book_id=self.available_book.owned_book_id
        )
        self.assertEqual(response.status_code, 400)
        self.assertNotEqual(not_updated_book.status, updated_status)

    def test_update_ownedbook_status_with_error_404_if_ownedbook_does_not_exist(self):
        updated_status = OwnedBook.OwnedBookStatus.NOT_VISIBLE
        update_data = {
            "status": updated_status,
        }
        self.available_book.delete()
        request = self.factory.patch(
            f"owned-books/{self.available_book.owned_book_id}/", update_data
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.available_book.owned_book_id
        )
        self.assertEqual(response.status_code, 404)

    def test_update_ownedbook_status_with_error_403_if_request_user_is_not_the_owner(
        self,
    ):
        updated_status = OwnedBook.OwnedBookStatus.NOT_VISIBLE
        update_data = {
            "status": updated_status,
        }
        request = self.factory.patch(
            f"owned-books/{self.available_book.owned_book_id}/", update_data
        )
        force_authenticate(request, self.another_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.available_book.owned_book_id
        )
        not_updated_book = OwnedBook.objects.get(
            owned_book_id=self.available_book.owned_book_id
        )
        self.assertEqual(response.status_code, 403)
        self.assertNotEqual(not_updated_book.status, updated_status)

    def test_update_ownedbook_status_from_not_visible_to_available_with_success(self):
        updated_status = OwnedBook.OwnedBookStatus.AVAILABLE
        update_data = {
            "status": updated_status,
        }
        request = self.factory.patch(
            f"owned-books/{self.not_visible_book.owned_book_id}/", update_data
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.not_visible_book.owned_book_id
        )
        updated_book = OwnedBook.objects.get(
            owned_book_id=self.not_visible_book.owned_book_id
        )
        resource_location = f"/owned-books/{self.not_visible_book.owned_book_id}/"
        self.assertEqual(response.status_code, 204)
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(updated_book.status, updated_status)

    def test_update_ownedbook_status_from_loaned_to_available_with_success_when_loan_status_is_return_confirmed(
        self,
    ):
        Loan.objects.filter(loan_id=self.ongoing_loan.loan_id).update(
            status=Loan.LoanStatus.RETURN_CONFIRMED
        )
        updated_status = OwnedBook.OwnedBookStatus.AVAILABLE
        update_data = {
            "status": updated_status,
        }
        request = self.factory.patch(
            f"owned-books/{self.loaned_book.owned_book_id}/", update_data
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.loaned_book.owned_book_id
        )
        updated_book = OwnedBook.objects.get(
            owned_book_id=self.loaned_book.owned_book_id
        )
        resource_location = f"/owned-books/{self.loaned_book.owned_book_id}/"
        self.assertEqual(response.status_code, 204)
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(updated_book.status, updated_status)

    def test_update_ownedbook_status_from_loaned_to_available_with_error_400_if_ongoing_loan(
        self,
    ):
        # ongoing loan = status of loan is loan_active, return_requested or return_pending
        for test_loan_status in [
            Loan.LoanStatus.LOAN_ACTIVE,
            Loan.LoanStatus.RETURN_REQUESTED,
            Loan.LoanStatus.RETURN_CONFIRMATION_PENDING,
        ]:
            Loan.objects.filter(loan_id=self.ongoing_loan.loan_id).update(
                status=test_loan_status
            )
            updated_status = OwnedBook.OwnedBookStatus.AVAILABLE
            update_data = {
                "status": updated_status,
            }
            request = self.factory.patch(
                f"owned-books/{self.loaned_book.owned_book_id}/", update_data
            )
            force_authenticate(request, self.owner_user)
            response = ModifyOwnedBookView.as_view()(
                request, owned_book_id=self.loaned_book.owned_book_id
            )
            not_updated_book = OwnedBook.objects.get(
                owned_book_id=self.loaned_book.owned_book_id
            )
            error_message = "Un prêt est toujours en cours pour ce livre, vous ne pouvez pas changer son statut."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))
            self.assertNotEqual(not_updated_book.status, updated_status)

    def test_update_ownedbook_status_from_loaned_to_not_visible_with_success_when_loan_status_is_return_confirmed(
        self,
    ):
        Loan.objects.filter(loan_id=self.ongoing_loan.loan_id).update(
            status=Loan.LoanStatus.RETURN_CONFIRMED
        )
        updated_status = OwnedBook.OwnedBookStatus.NOT_VISIBLE
        update_data = {
            "status": updated_status,
        }
        request = self.factory.patch(
            f"owned-books/{self.loaned_book.owned_book_id}/", update_data
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.loaned_book.owned_book_id
        )
        updated_book = OwnedBook.objects.get(
            owned_book_id=self.loaned_book.owned_book_id
        )
        resource_location = f"/owned-books/{self.loaned_book.owned_book_id}/"
        self.assertEqual(response.status_code, 204)
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(updated_book.status, updated_status)

    def test_update_ownedbook_status_from_loaned_to_not_visible_with_error_if_ongoing_loan(
        self,
    ):
        # ongoing loan = status of loan is loan_active, return_requested or return_pending
        for test_loan_status in [
            Loan.LoanStatus.LOAN_ACTIVE,
            Loan.LoanStatus.RETURN_REQUESTED,
            Loan.LoanStatus.RETURN_CONFIRMATION_PENDING,
        ]:
            Loan.objects.filter(loan_id=self.ongoing_loan.loan_id).update(
                status=test_loan_status
            )
            updated_status = OwnedBook.OwnedBookStatus.NOT_VISIBLE
            update_data = {
                "status": updated_status,
            }
            request = self.factory.patch(
                f"owned-books/{self.loaned_book.owned_book_id}/", update_data
            )
            force_authenticate(request, self.owner_user)
            response = ModifyOwnedBookView.as_view()(
                request, owned_book_id=self.loaned_book.owned_book_id
            )
            not_updated_book = OwnedBook.objects.get(
                owned_book_id=self.loaned_book.owned_book_id
            )
            error_message = "Un prêt est toujours en cours pour ce livre, vous ne pouvez pas changer son statut."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))
            self.assertNotEqual(not_updated_book.status, updated_status)

    def test_update_ownedbook_status_from_available_to_loaned_with_error_400(self):
        updated_status = OwnedBook.OwnedBookStatus.LOANED
        update_data = {
            "status": updated_status,
        }
        request = self.factory.patch(
            f"owned-books/{self.available_book.owned_book_id}/", update_data
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.available_book.owned_book_id
        )
        not_updated_book = OwnedBook.objects.get(
            owned_book_id=self.available_book.owned_book_id
        )
        error_message = "Créez un prêt pour changer le statut de ce livre."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))
        self.assertNotEqual(not_updated_book.status, updated_status)

    def test_update_ownedbook_status_from_not_visible_to_loaned_with_error_400(self):
        updated_status = OwnedBook.OwnedBookStatus.LOANED
        update_data = {
            "status": updated_status,
        }
        request = self.factory.patch(
            f"owned-books/{self.not_visible_book.owned_book_id}/", update_data
        )
        force_authenticate(request, self.owner_user)
        response = ModifyOwnedBookView.as_view()(
            request, owned_book_id=self.not_visible_book.owned_book_id
        )
        not_updated_book = OwnedBook.objects.get(
            owned_book_id=self.not_visible_book.owned_book_id
        )
        error_message = "Créez un prêt pour changer le statut de ce livre."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))
        self.assertNotEqual(not_updated_book.status, updated_status)
