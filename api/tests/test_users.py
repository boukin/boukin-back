from smtplib import SMTPException
import api.factory
from django.core import mail
from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate
from api.views import (
    ActivateUserView,
    CreateUserView,
    CreateExternalUserView,
    GetUserView,
    UpdateUserView,
)
from api.factory import UserFactory
from api.models import User, Friendship
from unittest.mock import patch


class UserTest(TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.user_data = {
            "username": "username",
            "email": "email@test.com",
            "password": "Motd3passev@lide",
        }
        self.admin_user = User.objects.create(
            username="admin", email="admin@boukin.org", password="@dminM0tdepasse"
        )

    def test_get_user_with_success(self):
        request = self.factory.get("users/search/?username=admin")
        force_authenticate(request, self.admin_user)
        response = GetUserView.as_view()(request, username="admin")
        self.assertEqual(response.status_code, 200)

    def test_get_user_return_wanted_data(self):
        request = self.factory.get("users/search/?username=admin")
        force_authenticate(request, self.admin_user)
        response = GetUserView.as_view()(request, username="admin")
        wanted_data = {
            "username": self.admin_user.username,
            "user_id": str(self.admin_user.user_id),
        }
        self.assertEqual(response.data, wanted_data)

    def test_get_user_with_email_param_with_success(self):
        request = self.factory.get("users/search/?email=admin@boukin.org")
        force_authenticate(request, self.admin_user)
        response = GetUserView.as_view()(request, email="admin@boukin.org")
        self.assertEqual(response.status_code, 200)

    def test_get_user_with_404_if_user_does_not_exist_with_username(self):
        request = self.factory.get("users/search/?username=notanuser")
        force_authenticate(request, self.admin_user)
        response = GetUserView.as_view()(request, username="notanuser")
        self.assertEqual(response.status_code, 404)

    def test_get_user_with_404_if_user_does_not_exist_with_email(self):
        request = self.factory.get("users/search/?email=notanemail@boukin.org")
        force_authenticate(request, self.admin_user)
        response = GetUserView.as_view()(request, email="notanemail@boukin.org")
        self.assertEqual(response.status_code, 404)

    def test_get_user_with_400_bad_request_if_invalid_param(self):
        request = self.factory.get("users/search/?invalidparam=invalidvalue")
        force_authenticate(request, self.admin_user)
        response = GetUserView.as_view()(request, invalidparam="invalidvalue")
        self.assertEqual(response.status_code, 400)

    def test_post_user_with_success(self):
        request = self.factory.post("users/", self.user_data)
        response = CreateUserView.as_view()(request)
        self.assertEqual(response.status_code, 201)

    def test_post_user_with_invalid_password(self):
        self.user_data.update({"password": "not_valid"})
        request = self.factory.post("users/", self.user_data)
        response = CreateUserView.as_view()(request)
        self.assertEqual(response.status_code, 400)

    def test_post_already_existing_user(self):
        self.user_data.update({"email": "admin@boukin.org"})
        request = self.factory.post("users/", self.user_data)
        response = CreateUserView.as_view()(request)
        self.assertEqual(response.status_code, 400)

    def test_post_user_with_success_sends_email_to_the_not_yet_confirmed_registered_user(
        self,
    ):
        request = self.factory.post("users/", self.user_data)
        response = CreateUserView.as_view()(request)
        email_subject = "Votre inscription sur Boukin"
        self.assertEqual(response.status_code, 201)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, email_subject)
        self.assertEqual(mail.outbox[0].to[0], self.user_data["email"])

    @patch("api.views.send_mail", side_effect=SMTPException)
    def test_post_user_with_error_if_email_fails_to_send(self, mock_smtp_exception):
        request = self.factory.post("users/", self.user_data)
        response = CreateUserView.as_view()(request)
        self.assertEqual(response.status_code, 500)
        self.assertEqual(len(mail.outbox), 0)
        self.assertIn(
            "Erreur lors de l'envoi de l'email d'activation de compte :", response.data
        )


class ActivateUserTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.user_factory = UserFactory()
        self.user = self.user_factory.new()

    def test_activate_with_error_if_user_not_found(self):
        invalid_uuid = "af978abc-2c9e-4fb0-a8b3-150511d227ec"
        request = self.factory.get(f"/users/{invalid_uuid}/activation")
        response = ActivateUserView.as_view()(request, user_id=invalid_uuid)
        self.assertEqual(response.status_code, 404)

    def test_activate_with_success_activates_user_registration(self):
        request = self.factory.get(f"/users/{self.user.user_id}/activation")
        response = ActivateUserView.as_view()(request, user_id=self.user.user_id)
        updated_user = User.objects.get(user_id=self.user.user_id)
        expected_url = "https://boukin.adaschool.fr"
        self.assertEqual(response.status_code, 302)
        self.assertEqual(updated_user.has_email_confirmed, True)
        self.assertEqual(response["Location"], expected_url)


class UpdateUserTest(TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.user_password = "@dminM0tdepasse"
        self.user = User.objects.create(
            username="admin", email="admin@boukin.org", password=self.user_password
        )
        self.patch_username_data = {"username": "newMe"}

    def test_update_user_s_username_or_email_with_success(self):
        update_scenarios = [
            {"field": "username", "updated_data": "newMe"},
            {"field": "email", "updated_data": "newemail@test.com"},
        ]
        for scenario in update_scenarios:
            user_patch_data = {scenario["field"]: scenario["updated_data"]}
            request = self.factory.patch(f"users/{self.user.user_id}/", user_patch_data)
            force_authenticate(request, self.user)
            response = UpdateUserView.as_view()(request, user_id=self.user.user_id)
            updated_user = User.objects.get(user_id=self.user.user_id)
            self.assertEqual(response.status_code, 204)
            self.assertEqual(
                getattr(updated_user, scenario["field"]), scenario["updated_data"]
            )

    def test_update_user_s_password_with_success_and_saved_it_hashed(self):
        user_patch_data = {"password": "newV@lidM0tdepasse"}
        request = self.factory.patch(f"users/{self.user.user_id}/", user_patch_data)
        force_authenticate(request, self.user)
        response = UpdateUserView.as_view()(request, user_id=self.user.user_id)
        updated_user = User.objects.get(user_id=self.user.user_id)
        self.assertEqual(response.status_code, 204)
        self.assertNotEqual(updated_user.password, user_patch_data["password"])

    def test_update_user_data_with_success_204_and_returns_resource_location_in_headers(
        self,
    ):
        request = self.factory.patch(
            f"users/{self.user.user_id}/", self.patch_username_data
        )
        force_authenticate(request, self.user)
        response = UpdateUserView.as_view()(request, user_id=self.user.user_id)
        resource_location = f"/users/{self.user.user_id}/"
        self.assertEqual(response.status_code, 204)
        self.assertEqual(response["Content-Location"], resource_location)

    def test_update_user_email_with_error_400_if_invalid_email(self):
        invalid_email = "invalid_email"
        user_patch_data = {"email": invalid_email}
        request = self.factory.patch(f"users/{self.user.user_id}/", user_patch_data)
        force_authenticate(request, self.user)
        response = UpdateUserView.as_view()(request, user_id=self.user.user_id)
        self.assertEqual(response.status_code, 400)

    def test_update_user_password_with_error_400_if_invalid_password(self):
        invalid_password = "invalid_password"
        user_patch_data = {"password": invalid_password}
        request = self.factory.patch(f"users/{self.user.user_id}/", user_patch_data)
        force_authenticate(request, self.user)
        response = UpdateUserView.as_view()(request, user_id=self.user.user_id)
        self.assertEqual(response.status_code, 400)

    def test_update_user_data_with_error_400_if_request_body_has_other_field_than_password_email_or_username(
        self,
    ):
        invalid_update_scenarios = [
            {"field": "is_active", "updated_data": False},
            {
                "field": "user_id",
                "updated_data": "d113a8e5-285c-4515-b9a2-ccd4b601124f",
            },
            {"field": "role", "updated_data": User.UserRole.ADMIN},
            {"field": "invalid_field", "updated_data": "data"},
        ]
        for scenario in invalid_update_scenarios:
            user_patch_data = {scenario["field"]: scenario["updated_data"]}
            request = self.factory.patch(f"users/{self.user.user_id}/", user_patch_data)
            force_authenticate(request, self.user)
            response = UpdateUserView.as_view()(request, user_id=self.user.user_id)
            updated_user = User.objects.get(user_id=self.user.user_id)
            self.assertEqual(response.status_code, 400)
            self.assertEqual(
                response.data["message"],
                "Vous ne pouvez modifier que l'email, le mot de passe ou le nom d'utilisateur·rice",
            )
            if hasattr(updated_user, scenario["field"]):
                self.assertNotEqual(
                    getattr(updated_user, scenario["field"]), scenario["updated_data"]
                )

    def test_update_user_data_with_error_403_if_request_user_is_someone_else(self):
        another_user = api.factory.UserFactory().new()
        request = self.factory.patch(
            f"users/{self.user.user_id}/", self.patch_username_data
        )
        force_authenticate(request, another_user)
        response = UpdateUserView.as_view()(request, user_id=self.user.user_id)
        self.assertEqual(response.status_code, 403)

    def test_update_user_data_with_error_404_if_inexistant_user(self):
        self.user.delete()
        request = self.factory.patch(
            f"users/{self.user.user_id}/", self.patch_username_data
        )
        force_authenticate(request, self.user)
        response = UpdateUserView.as_view()(request, user_id=self.user.user_id)
        self.assertEqual(response.status_code, 404)


class ExternalUserTest(TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.registered_user = User.objects.create(
            username="registered_user",
            email="registered_user@boukin.org",
            password="r3Gistered_User",
        )
        self.external_user_data = {"username": "external_user"}

    def test_post_external_user_with_success(self):
        request = self.factory.post("users/external/", self.external_user_data)
        force_authenticate(request, self.registered_user)
        response = CreateExternalUserView.as_view()(request)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["role"], User.UserRole.EXTERNAL.label)
        self.assertEqual(response.data["username"], self.external_user_data["username"])
        self.assertEqual(
            response.data["message"], "Utilisateur-ice externe créé-e. Amitié créée."
        )

    def test_post_external_user_with_error_if_username_is_empty_in_request_body(self):
        self.external_user_data.update({"username": ""})
        request = self.factory.post("users/external", self.external_user_data)
        force_authenticate(request, self.registered_user)
        response = CreateExternalUserView.as_view()(request)
        self.assertEqual(response.status_code, 400)

    def test_post_external_user_should_create_active_friendship_between_request_user_and_external_user(
        self,
    ):
        request = self.factory.post("users/external", self.external_user_data)
        force_authenticate(request, self.registered_user)
        response = CreateExternalUserView.as_view()(request)
        created_friendship = Friendship.objects.get(
            requesting_user=self.registered_user.user_id
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            created_friendship.status, Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE
        )

    def test_post_multiple_external_users_with_success_with_null_value_for_email(self):
        first_request = self.factory.post("users/external", self.external_user_data)
        force_authenticate(first_request, self.registered_user)
        another_external_user_data = {"username": "another_external_user"}
        second_request = self.factory.post("users/external", another_external_user_data)
        force_authenticate(second_request, self.registered_user)
        CreateExternalUserView.as_view()(first_request)
        second_response = CreateExternalUserView.as_view()(second_request)
        self.assertEqual(second_response.status_code, 201)
        self.assertEqual(second_response.data["role"], User.UserRole.EXTERNAL.label)
        self.assertEqual(
            second_response.data["username"], another_external_user_data["username"]
        )
        self.assertEqual(
            second_response.data["message"],
            "Utilisateur-ice externe créé-e. Amitié créée.",
        )
