from datetime import datetime, timedelta
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from api.models import User, Book, OwnedBook, Loan, Friendship


class Authentication(APITestCase):

    def setUp(self):
        self.user_password = "IamJust4NorM@lUs3r"
        self.user = User.objects.create_user(
            username="normal_user",
            email="normal_user@boukin.org",
            password=self.user_password,
            has_email_confirmed=True,
        )
        self.get_token_data = {
            "email": self.user.email,
            "password": self.user_password,
        }
        self.another_user_password = "IamJust4Noth3rUs3r"
        self.another_user = User.objects.create_user(
            username="another_user",
            email="another_user@boukin.org",
            password=self.another_user_password,
            has_email_confirmed=True,
        )

    def test_create_token_with_success(self):
        response = self.client.post(
            "/auth-token/", data=self.get_token_data, format="json"
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["user_id"], self.user.user_id)
        self.assertIn("token", response.data)
        self.assertEqual(response.data["email"], self.user.email)
        self.assertEqual(response.data["username"], self.user.username)

    def test_create_token_with_success_for_user_created_by_post_request(self):
        user_data = {
            "username": "test_user",
            "email": "test_user@boukin.org",
            "password": "IamJust4NorM@lUs3r",
        }
        self.client.post("/users/", user_data)
        user = User.objects.filter(email=user_data["email"])
        user.update(has_email_confirmed=True)
        post_data = {
            "email": user_data["email"],
            "password": user_data["password"],
        }
        response = self.client.post("/auth-token/", data=post_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertIn("user_id", response.data)
        self.assertIn("token", response.data)
        self.assertEqual(response.data["email"], user_data["email"])
        self.assertEqual(response.data["username"], user_data["username"])

    def test_get_token_with_success_if_token_already_created(self):
        Token.objects.create(user=self.another_user)
        token_data = {
            "email": self.another_user.email,
            "password": self.another_user_password,
        }

        response = self.client.post("/auth-token/", data=token_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["user_id"], self.another_user.user_id)
        self.assertIn("token", response.data)
        self.assertEqual(response.data["email"], self.another_user.email)
        self.assertEqual(response.data["username"], self.another_user.username)

    def test_post_token_with_error_with_invalid_email_password_match(self):
        self.get_token_data["password"] = "Inv@lidPassW0rd"
        response = self.client.post(
            "/auth-token/", data=self.get_token_data, format="json"
        )
        self.assertEqual(response.status_code, 400)

    def test_post_token_with_error_for_unrecognized_email(self):
        self.get_token_data["email"] = "absent_user@boukin.org"
        self.get_token_data["password"] = "Inv@lidPassW0rd"
        response = self.client.post(
            "/auth-token/", data=self.get_token_data, format="json"
        )
        self.assertEqual(response.status_code, 400)


class Endpoints(APITestCase):

    def setUp(self):
        self.user_password = "IamJust4NorM@lUs3r"
        self.user = User.objects.create_user(
            username="normal_user",
            email="normal_user@boukin.org",
            password=self.user_password,
            has_email_confirmed=True,
        )
        self.another_user = User.objects.create_user(
            username="another_user",
            email="another_user@boukin.org",
            password="an0th3r_Us3r",
            has_email_confirmed=True,
        )
        self.yet_another_user = User.objects.create_user(
            username="yet_another_user",
            email="yet_another_user@boukin.org",
            password="Y3T_an0th3r_Us3r",
            has_email_confirmed=True,
        )
        self.get_token_data = {
            "email": self.user.email,
            "password": self.user_password,
        }
        self.login = self.client.post(
            "/auth-token/", data=self.get_token_data, format="json"
        )
        self.token = self.login.data["token"]
        self.book_data = {
            "title": "Les Vagues",
            "author_fname": "Virginia",
            "author_lname": "Woolf",
            "isbn": "9780123456789",
        }
        self.friendship = Friendship.objects.create(
            requesting_user=self.user,
            addressed_user=self.another_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )
        self.registered_book = Book.objects.create(
            title="Orlando",
            isbn="9789876543210",
            author_fname=self.book_data["author_fname"],
            author_lname=self.book_data["author_lname"],
        )
        self.ownedbook = OwnedBook.objects.create(
            owner=self.user, book=self.registered_book
        )
        self.loan_active = Loan.objects.create(
            owner=self.user,
            borrower=self.another_user,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.LOAN_ACTIVE,
        )
        self.loan_denied = Loan.objects.create(
            owner=self.user,
            borrower=self.another_user,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.LOAN_DENIED,
        )
        self.loan_requested = Loan.objects.create(
            owner=self.user,
            borrower=self.another_user,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.LOAN_REQUESTED,
        )
        self.today = datetime.today().strftime("%Y-%m-%d")
        self.one_week_later = (datetime.today() + timedelta(weeks=1)).strftime(
            "%Y-%m-%d"
        )

    def test_search_user_with_success_when_logged_in_and_errors_when_logged_out_or_using_bad_token(
        self,
    ):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.get("/users/search/?username=another_user")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["username"], "another_user")
        self.assertEqual(response.data["user_id"], str(self.another_user.user_id))
        self.client.credentials()
        bad_response = self.client.get("/users/search/?username=another_user")
        self.assertEqual(bad_response.status_code, 401)
        self.assertEqual(
            bad_response.json()["detail"],
            "Authentication credentials were not provided.",
        )
        self.client.credentials(HTTP_AUTHORIZATION="Token " + "bad_token")
        worst_response = self.client.get("/users/search/?username=another_user")
        self.assertEqual(worst_response.status_code, 401)
        self.assertEqual(worst_response.json()["detail"], "Invalid token.")

    def test_post_user_external_with_success_when_logged_in_when_loaning_a_book_to_an_unregistered_friend(
        self,
    ):
        external_user_data = {"username": "Bob"}
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post("/users/external/", external_user_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.json()["message"], "Utilisateur-ice externe créé-e. Amitié créée."
        )

    def test_post_book_with_success_to_create_a_reference_of_a_book_when_logged_in(
        self,
    ):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post("/books/", self.book_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["title"], "Les Vagues")
        self.assertEqual(response.data["author_fname"], "Virginia")
        self.assertEqual(response.data["author_lname"], "Woolf")

    def test_post_owned_book_with_success_to_own_a_copy_of_a_book_when_logged_in(self):
        owned_book_data = {
            "owner": self.user.user_id,
            "book": self.registered_book.book_id,
            "status": OwnedBook.OwnedBookStatus.AVAILABLE,
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post("/owned-books/", owned_book_data)
        self.assertEqual(response.status_code, 201)
        self.assertIn("owned_book_id", response.data)
        self.assertIn("owner", response.data)
        self.assertEqual(response.data["status"], OwnedBook.OwnedBookStatus.AVAILABLE)

    def test_delete_owned_book_with_success_when_logged_in(self):
        owned_book = OwnedBook.objects.create(
            owner=self.user,
            book=self.registered_book,
            status=OwnedBook.OwnedBookStatus.AVAILABLE,
        )
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.delete(f"/owned-books/{owned_book.owned_book_id}/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), "Livre supprimé.")

    def test_update_owned_book_to_not_visible_with_success_when_logged_in(self):
        owned_book = OwnedBook.objects.create(
            owner=self.user,
            book=self.registered_book,
            status=OwnedBook.OwnedBookStatus.AVAILABLE,
        )
        owned_book_to_update_data = {
            "status": OwnedBook.OwnedBookStatus.NOT_VISIBLE,
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.patch(
            f"/owned-books/{owned_book.owned_book_id}/", owned_book_to_update_data
        )
        self.assertEqual(response.status_code, 204)

    def test_get_user_owned_book_with_success_to_see_our_own_books_when_logged_in(self):
        another_registered_book = Book.objects.create(
            title="La Promenade au Phare",
            isbn="9788547986415",
            author_fname=self.book_data["author_fname"],
            author_lname=self.book_data["author_lname"],
        )
        OwnedBook.objects.create(
            owner=self.user,
            book=another_registered_book,
            status=OwnedBook.OwnedBookStatus.AVAILABLE,
        )
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        owner_data = {"owner": self.user.user_id}
        response = self.client.get("/owned-books/", owner_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]["book"]["title"], "Orlando")
        self.assertEqual(response.data[1]["book"]["title"], "La Promenade au Phare")

    def test_get_friend_s_owned_books_with_success_when_logged_in(
        self,
    ):
        registered_book = Book.objects.create(
            title="Mrs. Dolloway",
            isbn="9788547986415",
            author_fname=self.book_data["author_fname"],
            author_lname=self.book_data["author_lname"],
        )
        another_registered_book = Book.objects.create(
            title="La Traversée des Apparences",
            isbn="978789123456",
            author_fname=self.book_data["author_fname"],
            author_lname=self.book_data["author_lname"],
        )
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        OwnedBook.objects.create(
            owner=self.another_user,
            book=registered_book,
            status=OwnedBook.OwnedBookStatus.AVAILABLE,
        )
        OwnedBook.objects.create(
            owner=self.another_user,
            book=another_registered_book,
            status=OwnedBook.OwnedBookStatus.AVAILABLE,
        )
        response = self.client.get(f"/users/{self.another_user.user_id}/owned-books/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]["book"]["title"], "Mrs. Dolloway")
        self.assertEqual(
            response.data[1]["book"]["title"], "La Traversée des Apparences"
        )

    def test_post_loan_to_create_loan_as_book_owner_with_success_when_logged_in(self):
        registered_owned_book = OwnedBook.objects.create(
            owner=self.user,
            book=self.registered_book,
            status=OwnedBook.OwnedBookStatus.AVAILABLE,
        )
        loan_data = {
            "owner": self.user.user_id,
            "borrower": self.another_user.user_id,
            "owned_book": registered_owned_book.owned_book_id,
            "status": Loan.LoanStatus.LOAN_ACTIVE,
            "starting_date": self.today,
            "ending_date": self.one_week_later,
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post("/loans/", loan_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["status"], Loan.LoanStatus.LOAN_ACTIVE)

    def test_post_loan_request_to_ask_for_a_loan_to_a_friend_when_logged_in(self):
        registered_owned_book = OwnedBook.objects.create(
            owner=self.another_user,
            book=self.registered_book,
            status=OwnedBook.OwnedBookStatus.AVAILABLE,
        )
        loan_data = {
            "owner": self.another_user.user_id,
            "borrower": self.user.user_id,
            "owned_book": registered_owned_book.owned_book_id,
            "starting_date": self.today,
            "ending_date": self.one_week_later,
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post("/loans/request/", loan_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["status"], Loan.LoanStatus.LOAN_REQUESTED)

    def test_update_loan_with_success_to_loan_active_when_logged_in(self):
        registered_owned_book = OwnedBook.objects.create(
            owner=self.user,
            book=self.registered_book,
            status=OwnedBook.OwnedBookStatus.LOANED,
        )
        existing_loan = Loan.objects.create(
            owner=self.user,
            borrower=self.another_user,
            owned_book=registered_owned_book,
            status=Loan.LoanStatus.LOAN_REQUESTED,
        )
        update_loan_data = {"status": Loan.LoanStatus.LOAN_ACTIVE}
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.patch(
            f"/loans/{existing_loan.loan_id}/", update_loan_data
        )
        self.assertEqual(response.status_code, 204)

    def test_post_friendship_with_success_to_request_friendship(self):
        friendship_data = {
            "requesting_user": self.user.user_id,
            "addressed_user": self.yet_another_user.user_id,
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post("/friendships/", friendship_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data["status"], Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED
        )

    def test_delete_friendship_with_success(self):
        friendship_data = {
            "requesting_user": self.user.user_id,
            "addressed_user": self.yet_another_user.user_id,
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post("/friendships/", friendship_data)
        friendship_requested_id = response.data["friendship_id"]
        response = self.client.delete(f"/friendships/{friendship_requested_id}/")
        self.assertEqual(response.status_code, 200)
        with self.assertRaises(Friendship.DoesNotExist):
            Friendship.objects.get(friendship_id=friendship_requested_id)

    def test_get_one_book_registered_in_database_by_isbn_with_success(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.get(f"/books/?isbn={self.registered_book.isbn}")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]["isbn"], self.registered_book.isbn)

    def test_all_loans_with_success_when_logged_in(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.get(f"/users/{self.user.user_id}/loans/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data["loans"][0]["owned_book"]["book"]["title"],
            self.registered_book.title,
        )
        self.assertEqual(
            response.data["loans"][0]["borrower"]["username"],
            self.another_user.username,
        )

    def test_get_user_friendships_with_success_when_logged_in(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.get(f"/users/{self.user.user_id}/friendships/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data[0]["friendship_id"],
            str(self.friendship.friendship_id),
        )

    def test_update_user_data_with_success_when_logged_in(self):
        updated_username = "updated_username"
        update_user_data = {"username": updated_username}
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.patch(f"/users/{self.user.user_id}/", update_user_data)
        updated_user = User.objects.get(user_id=self.user.user_id)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(updated_user.username, updated_username)
