from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate
from api.views import BooksView

from api.models import User, Book


class CreateBookTest(TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.book_data = {
            "title": "titre",
            "author_fname": "Jane",
            "author_lname": "Doe",
            "isbn": "9780123456789",
        }
        self.already_existing_book = Book.objects.create(
            title="titre", author_fname="John", author_lname="Doe", isbn="9781237894560"
        )
        self.admin_user = User.objects.create(
            username="admin", email="admin@boukin.org", password="@dminM0tdepasse"
        )

    def test_post_book_with_success(self):
        request = self.factory.post("books/", self.book_data)
        force_authenticate(request, self.admin_user)
        response = BooksView.as_view()(request)
        self.assertEqual(response.status_code, 201)

    def test_post_already_existing_book(self):
        self.book_data.update({"isbn": "9781237894560"})
        request = self.factory.post("books/", self.book_data)
        force_authenticate(request, self.admin_user)
        response = BooksView.as_view()(request)
        self.assertEqual(response.status_code, 400)

    def test_post_book_with_incorrect_isbn(self):
        self.book_data.update({"isbn": "9781054"})
        request = self.factory.post("books/", self.book_data)
        force_authenticate(request, self.admin_user)
        response = BooksView.as_view()(request)
        self.assertEqual(response.status_code, 400)


class BookSimpleSearch(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.book = Book.objects.create(
            title="Title to research",
            author_fname="John",
            author_lname="Doe",
            isbn="9781237894561",
        )
        self.book_of_namesake_author = Book.objects.create(
            title="A complete different book",
            author_fname="Mary",
            author_lname="Doe",
            isbn="9781237894562",
        )
        self.user = User.objects.create_user(
            username="normal_user",
            email="normal_user@boukin.org",
            password="IamJust4NorM@lUs3r",
            has_email_confirmed=True,
        )

    def test_get_one_book_registered_in_database_by_isbn_with_success(self):
        searched_value = self.book.isbn
        request = self.factory.get(f"books/?isbn={searched_value}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, isbn=searched_value)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["isbn"], self.book.isbn)
        self.assertEqual(response.data[0]["title"], self.book.title)

    def test_get_one_book_registered_in_database_by_exact_title_with_success(self):
        searched_value = self.book.title
        request = self.factory.get(f"books/?title={searched_value}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, title=searched_value)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["isbn"], self.book.isbn)
        self.assertEqual(response.data[0]["title"], self.book.title)

    def test_get_one_book_registered_in_database_by_case_insensitive_contained_title_with_success(
        self,
    ):
        searched_value = "to rEseaRch"
        request = self.factory.get(f"books/?title={searched_value}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, title=searched_value)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]["isbn"], self.book.isbn)
        self.assertEqual(response.data[0]["title"], self.book.title)

    def test_get_one_book_registered_in_database_by_title_surrounded_by_spaces_with_success(
        self,
    ):
        searched_value = " " + self.book.title + "  "
        request = self.factory.get(f"books/?title={searched_value}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, title=searched_value)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]["isbn"], self.book.isbn)
        self.assertEqual(response.data[0]["title"], self.book.title)

    def test_get_several_books_of_namesakes_authors_by_exact_last_name_with_success(
        self,
    ):
        searched_value = self.book.author_lname
        request = self.factory.get(f"books/?author_lname={searched_value}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, author_lname=searched_value)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]["isbn"], self.book.isbn)
        self.assertEqual(response.data[0]["title"], self.book.title)
        self.assertEqual(response.data[1]["isbn"], self.book_of_namesake_author.isbn)
        self.assertEqual(response.data[1]["title"], self.book_of_namesake_author.title)

    def test_get_one_book_by_author_exact_fname_and_lname_with_success(self):
        author_fname = self.book_of_namesake_author.author_fname
        author_lname = self.book_of_namesake_author.author_lname
        request = self.factory.get(
            f"books/?author_lname={author_lname}&author_fname={author_fname}"
        )
        force_authenticate(request, self.user)
        response = BooksView.as_view()(
            request, author_lname=author_lname, author_fname=author_fname
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["isbn"], self.book_of_namesake_author.isbn)
        self.assertEqual(response.data[0]["title"], self.book_of_namesake_author.title)

    def test_get_one_book_by_author_case_insensitive_fname_and_lname_with_success(self):
        author_fname = "MaRy"
        author_lname = "DOE"
        request = self.factory.get(
            f"books/?author_lname={author_lname}&author_fname={author_fname}"
        )
        force_authenticate(request, self.user)
        response = BooksView.as_view()(
            request, author_lname=author_lname, author_fname=author_fname
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]["isbn"], self.book_of_namesake_author.isbn)
        self.assertEqual(response.data[0]["title"], self.book_of_namesake_author.title)

    def test_search_by_isbn_only_if_isbn_among_other_query_parameters(self):
        isbn = self.book.isbn
        author_lname = "inexistent_author"
        request = self.factory.get(f"books/?isbn={isbn}&author_lname={author_lname}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, isbn=isbn, author_lname=author_lname)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["isbn"], self.book.isbn)
        self.assertEqual(response.data[0]["title"], self.book.title)

    def test_get_one_book_with_error_401_if_request_user_not_authenticated(self):
        searched_value = self.book.isbn
        request = self.factory.get(f"books/?isbn={searched_value}")
        response = BooksView.as_view()(request, isbn=searched_value)
        self.assertEqual(response.status_code, 401)

    def test_get_one_book_with_error_if_only_fname_passed_as_query_parameter(self):
        searched_value = self.book.author_fname
        request = self.factory.get(f"books/?author_fname={searched_value}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, author_fname=searched_value)
        self.assertEqual(response.status_code, 400)

    def test_get_one_book_with_error_400_if_wrong_query_parameters(self):
        searched_value = "wrong value"
        request = self.factory.get(f"books/?wrong_parameter={searched_value}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, wrong_parameter=searched_value)
        self.assertEqual(response.status_code, 400)

    def test_get_one_book_with_error_400_if_search_by_title_with_empty_title(self):
        empty_value = "   "
        request = self.factory.get(f"books/?title={empty_value}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, title=empty_value)
        self.assertEqual(response.status_code, 400)

    def test_get_one_book_with_error_404_if_no_book_corresponding_to_request_information(
        self,
    ):
        self.book.delete()
        # by isbn
        searched_value = self.book.isbn
        request = self.factory.get(f"books/?isbn={searched_value}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, isbn=searched_value)
        self.assertEqual(response.status_code, 404)
        # by title
        searched_value = self.book.title
        request = self.factory.get(f"books/?title={searched_value}")
        force_authenticate(request, self.user)
        response = BooksView.as_view()(request, title=searched_value)
        self.assertEqual(response.status_code, 404)
        # by author
        author_fname = self.book.author_fname
        author_lname = self.book.author_lname
        request = self.factory.get(
            f"books/?author_lname={author_lname}&author_fname={author_fname}"
        )
        force_authenticate(request, self.user)
        response = BooksView.as_view()(
            request, author_lname=searched_value, author_fname=author_fname
        )
        self.assertEqual(response.status_code, 404)
