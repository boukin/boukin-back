from django_test_migrations.contrib.unittest_case import MigratorTestCase


class TestMigrations(MigratorTestCase):

    migrate_from = ("api", "0007_user_has_email_confirmed")
    migrate_to = ("api", "0008_authenticate_registered_users")

    def prepare(self):
        Users = self.old_state.apps.get_model("api", "User")
        Users.objects.create(
            username="someuser", email="someuser@boukin.org", password="Password1@"
        )

    def test_migration_for_authenticating_already_registered_users(self):

        Users = self.new_state.apps.get_model("api", "User")
        old_user = Users.objects.get(username="someuser")
        new_user = Users.objects.create(
            username="anewuser", email="anewuser@boukin.org", password="Password1@"
        )

        self.assertEqual(Users.objects.count(), 2)
        self.assertEqual(old_user.has_email_confirmed, True)
        self.assertEqual(new_user.has_email_confirmed, False)
