import api.factory
from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate
from api.views import (
    CreateFriendshipView,
    ModifyFriendshipView,
    GetUserFriendshipsView,
    GetUserSingleFriendshipView,
)
from api.models import User, Friendship


class CreateFriendship(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.request_user = User.objects.create(
            username="user1", email="user1@boukin.org", password="@dminM0tdepasse"
        )
        self.target_user = User.objects.create(
            username="user2", email="user2@boukin.org", password="@dminM0tdepasse"
        )
        self.friend_user = User.objects.create(
            username="user3", email="user3@boukin.org", password="@dminM0tdepasse"
        )
        self.existing_friendship = Friendship.objects.create(
            requesting_user=self.request_user,
            addressed_user=self.friend_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )
        self.friendship_request = {
            "requesting_user": self.request_user.user_id,
            "addressed_user": self.target_user.user_id,
        }

    def test_post_friendship_with_success(self):
        request = self.factory.post("friendships/", self.friendship_request)
        force_authenticate(request, self.request_user)
        response = CreateFriendshipView.as_view()(request)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data["status"], Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED
        )

    def test_post_friendship_with_error_if_self_request(self):
        self.friendship_request.update({"addressed_user": self.request_user.user_id})
        request = self.factory.post("friendships/", self.friendship_request)
        force_authenticate(request, self.request_user)
        response = CreateFriendshipView.as_view()(request)
        self.assertEqual(response.status_code, 400)

    def test_post_friendship_with_error_if_target_not_in_database(self):
        self.target_user.delete()
        request = self.factory.post("friendships/", self.friendship_request)
        force_authenticate(request, self.request_user)
        response = CreateFriendshipView.as_view()(request)
        self.assertEqual(response.status_code, 400)

    def test_post_friendship_with_error_if_friendship_already_exists(self):
        self.friendship_request.update({"addressed_user": self.friend_user.user_id})
        request = self.factory.post("friendships/", self.friendship_request)
        force_authenticate(request, self.request_user)
        response = CreateFriendshipView.as_view()(request)
        error_message = "Vous êtes déjà ami-e avec cette personne."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_post_friendship_with_error_if_reversed_friendship_exists(self):
        self.friendship_request.update(
            {
                "requesting_user": self.friend_user.user_id,
                "addressed_user": self.request_user.user_id,
            }
        )
        request = self.factory.post("friendships/", self.friendship_request)
        force_authenticate(request, self.request_user)
        response = CreateFriendshipView.as_view()(request)
        error_message = "Vous êtes déjà ami-e avec cette personne."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_post_friendship_from_A_to_B_with_error_if_request_already_exists_from_B_to_A(
        self,
    ):
        user_A = User.objects.create(
            username="A", email="mailA@mail.com", password="P@ssw0rd!"
        )
        user_B = User.objects.create(
            username="B", email="mailB@mail.com", password="P@ssw0rd!"
        )
        self.existing_friendship_request = Friendship.objects.create(
            requesting_user=user_B,
            addressed_user=user_A,
            status=Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        )

        self.friendship_request.update(
            {
                "requesting_user": user_A.user_id,
                "addressed_user": user_B.user_id,
            }
        )
        request = self.factory.post("friendships/", self.friendship_request)
        force_authenticate(request, self.request_user)
        response = CreateFriendshipView.as_view()(request)
        error_message = "Une demande d'amitié est déjà en cours."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_post_friendship_from_A_to_B_with_error_if_request_already_exists(self):
        user_A = User.objects.create(
            username="A", email="mailA@mail.com", password="P@ssw0rd!"
        )
        user_B = User.objects.create(
            username="B", email="mailB@mail.com", password="P@ssw0rd!"
        )
        self.existing_friendship_request = Friendship.objects.create(
            requesting_user=user_A,
            addressed_user=user_B,
            status=Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        )

        self.friendship_request.update(
            {
                "requesting_user": user_A.user_id,
                "addressed_user": user_B.user_id,
            }
        )
        request = self.factory.post("friendships/", self.friendship_request)
        force_authenticate(request, self.request_user)
        response = CreateFriendshipView.as_view()(request)
        error_message = "Une demande d'amitié est déjà en cours."
        self.assertIn(error_message, str(response.data))
        self.assertEqual(response.status_code, 400)

    def test_post_friendship_with_error_if_friendship_request_already_denied(self):
        user_A = User.objects.create(
            username="A", email="mailA@mail.com", password="P@ssw0rd!"
        )
        user_B = User.objects.create(
            username="B", email="mailB@mail.com", password="P@ssw0rd!"
        )
        self.existing_friendship_request = Friendship.objects.create(
            requesting_user=user_A,
            addressed_user=user_B,
            status=Friendship.FriendshipStatus.FRIENDSHIP_DENIED,
        )

        self.friendship_request.update(
            {
                "requesting_user": user_A.user_id,
                "addressed_user": user_B.user_id,
            }
        )
        request = self.factory.post("friendships/", self.friendship_request)
        force_authenticate(request, self.request_user)
        response = CreateFriendshipView.as_view()(request)
        error_message = "Vous ne pouvez demander cette personne en ami-e."
        self.assertIn(error_message, str(response.data))
        self.assertEqual(response.status_code, 400)

    def test_post_friendship_with_error_if_friendship_is_over(self):
        user_A = User.objects.create(
            username="A", email="mailA@mail.com", password="P@ssw0rd!"
        )
        user_B = User.objects.create(
            username="B", email="mailB@mail.com", password="P@ssw0rd!"
        )
        self.existing_friendship_request = Friendship.objects.create(
            requesting_user=user_A,
            addressed_user=user_B,
            status=Friendship.FriendshipStatus.FRIENDSHIP_OVER,
        )

        self.friendship_request.update(
            {
                "requesting_user": user_A.user_id,
                "addressed_user": user_B.user_id,
            }
        )
        request = self.factory.post("friendships/", self.friendship_request)
        force_authenticate(request, self.request_user)
        response = CreateFriendshipView.as_view()(request)
        error_message = "Vous ne pouvez demander cette personne en ami-e."
        self.assertIn(error_message, str(response.data))
        self.assertEqual(response.status_code, 400)


class DeleteFriendship(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.requesting_user = User.objects.create(
            username="user1", email="user1@boukin.org", password="@dminM0tdepasse"
        )
        self.addressed_user = User.objects.create(
            username="user2", email="user2@boukin.org", password="@dminM0tdepasse"
        )
        self.friend_user = User.objects.create(
            username="user3", email="user3@boukin.org", password="@dminM0tdepasse"
        )
        self.denied_user = User.objects.create(
            username="user4", email="user4@boukin.org", password="@dminM0tdepasse"
        )
        self.over_user = User.objects.create(
            username="user5", email="user5@boukin.org", password="@dminM0tdepasse"
        )
        self.existing_friendship_requested = Friendship.objects.create(
            requesting_user=self.requesting_user,
            addressed_user=self.addressed_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        )
        self.existing_friendship_active = Friendship.objects.create(
            requesting_user=self.requesting_user,
            addressed_user=self.friend_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )
        self.existing_friendship_denied = Friendship.objects.create(
            requesting_user=self.requesting_user,
            addressed_user=self.denied_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_DENIED,
        )
        self.existing_friendship_over = Friendship.objects.create(
            requesting_user=self.requesting_user,
            addressed_user=self.over_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_OVER,
        )

    def test_delete_friendship_when_requesting_user_deletes_his_own_request_with_success(
        self,
    ):
        request = self.factory.delete(
            f"friendships/{self.existing_friendship_requested.friendship_id}",
        )
        force_authenticate(request, self.requesting_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_requested.friendship_id
        )
        message = "Amitié supprimée."
        self.assertEqual(response.status_code, 200)
        self.assertIn(message, str(response.data))
        with self.assertRaises(Friendship.DoesNotExist):
            Friendship.objects.get(
                friendship_id=self.existing_friendship_requested.friendship_id
            )

    def test_delete_friendship_by_anyone_but_requesting_user_with_error(self):
        other_users = [
            self.addressed_user,
            self.friend_user,
        ]
        request = self.factory.delete(
            f"friendships/{self.existing_friendship_requested.friendship_id}",
        )
        for user in other_users:
            force_authenticate(request, user)
            response = ModifyFriendshipView.as_view()(
                request, friendship_id=self.existing_friendship_requested.friendship_id
            )
            self.assertEqual(response.status_code, 403)
            current_friendship = Friendship.objects.get(
                friendship_id=self.existing_friendship_requested.friendship_id
            )
            self.assertEqual(
                current_friendship.status,
                Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
            )

    def test_delete_friendship_by_requesting_user_with_error_when_any_status_else_than_requested(
        self,
    ):
        friendships = [
            self.existing_friendship_active,
            self.existing_friendship_denied,
            self.existing_friendship_over,
        ]

        for friendship in friendships:
            request = self.factory.delete(
                f"friendships/{friendship.friendship_id}",
            )
            force_authenticate(request, self.requesting_user)
            response = ModifyFriendshipView.as_view()(
                request, friendship_id=friendship.friendship_id
            )
            error_message = "Vous ne pouvez supprimer une amitié en cours."
            self.assertEqual(response.status_code, 403)
            self.assertIn(error_message, str(response.data))
            current_friendship = Friendship.objects.get(
                friendship_id=friendship.friendship_id
            )
            self.assertEqual(current_friendship.status, friendship.status)

    def test_delete_friendship_with_error_if_friendship_does_not_exist(self):
        self.existing_friendship_requested.delete()
        request = self.factory.delete(
            f"friendships/{self.existing_friendship_requested.friendship_id}",
        )
        force_authenticate(request, self.requesting_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_requested.friendship_id
        )
        error_message = (
            "Vous n'avez pas de demande d'amitié en cours avec cette personne."
        )
        self.assertEqual(response.status_code, 404)
        self.assertIn(error_message, str(response.data))


class UpdateFriendship(TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.requesting_user = User.objects.create(
            username="user1", email="user1@boukin.org", password="@dminM0tdepasse"
        )
        self.addressed_user = User.objects.create(
            username="user2", email="user2@boukin.org", password="@dminM0tdepasse"
        )
        self.friend_user = User.objects.create(
            username="user3", email="user3@boukin.org", password="@dminM0tdepasse"
        )
        self.denied_user = User.objects.create(
            username="user4", email="user4@boukin.org", password="@dminM0tdepasse"
        )
        self.existing_friendship_requested = Friendship.objects.create(
            requesting_user=self.requesting_user,
            addressed_user=self.addressed_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        )
        self.existing_friendship_active = Friendship.objects.create(
            requesting_user=self.requesting_user,
            addressed_user=self.friend_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )
        self.existing_friendship_denied = Friendship.objects.create(
            requesting_user=self.requesting_user,
            addressed_user=self.denied_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_DENIED,
        )
        self.existing_friendship_over = Friendship.objects.create(
            requesting_user=self.friend_user,
            addressed_user=self.addressed_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_OVER,
        )

    def test_update_friendship_with_error_if_friendship_does_not_exist(self):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        }
        self.existing_friendship_requested.delete()
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_requested.friendship_id}",
            request_data,
        )
        force_authenticate(request, self.addressed_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_requested.friendship_id
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(
            response.data,
            "Vous n'avez pas de demande d'amitié en cours avec cette personne.",
        )

    def test_update_friendship_with_error_if_updated_by_another_user(self):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_requested.friendship_id}",
            request_data,
        )
        force_authenticate(request, self.friend_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_requested.friendship_id
        )
        self.assertEqual(response.status_code, 403)

    def test_update_friendship_status_from_requested_to_any_with_error_when_updated_by_requesting_user(
        self,
    ):
        for status in Friendship.FriendshipStatus:
            request_data = {
                "status": status,
            }
            request = self.factory.patch(
                f"friendships/{self.existing_friendship_requested.friendship_id}",
                request_data,
            )
            force_authenticate(request, self.requesting_user)
            response = ModifyFriendshipView.as_view()(
                request, friendship_id=self.existing_friendship_requested.friendship_id
            )
            error_message = "Seul l'utilisateur qui a reçu la demande peut modifier le statut de cette amitié."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_from_denied_to_any_with_error_when_updated_by_requesting_user(
        self,
    ):
        for status in Friendship.FriendshipStatus:
            request_data = {
                "status": status,
            }
            request = self.factory.patch(
                f"friendships/{self.existing_friendship_denied.friendship_id}",
                request_data,
            )
            force_authenticate(request, self.requesting_user)
            response = ModifyFriendshipView.as_view()(
                request, friendship_id=self.existing_friendship_denied.friendship_id
            )
            error_message = "Votre demande d'amitié a été refusée. Vous ne pouvez pas modifier le statut de cette amitié."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_from_active_to_requested_by_requesting_user_with_error(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_active.friendship_id}", request_data
        )
        force_authenticate(request, self.requesting_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_active.friendship_id
        )
        error_message = "Il n'est pas possible de passer d'une amitié active à une demande d'amitié."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_from_active_to_denied_by_requesting_user_with_error(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_DENIED,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_active.friendship_id}", request_data
        )
        force_authenticate(request, self.requesting_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_active.friendship_id
        )
        error_message = "Il n'est pas possible de passer d'une amitié active à refusée. Vous pouvez néanmoins mettre fin à cette amitié."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_from_active_to_active_by_requesting_user_with_error(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_active.friendship_id}", request_data
        )
        force_authenticate(request, self.requesting_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_active.friendship_id
        )
        error_message = "Votre amitié avec cette personne est déjà active."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_from_active_to_over_by_requesting_user_with_success(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_OVER,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_active.friendship_id}", request_data
        )
        force_authenticate(request, self.requesting_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_active.friendship_id
        )
        friendship = Friendship.objects.get(
            friendship_id=self.existing_friendship_active.friendship_id
        )
        resource_location = (
            f"/friendships/{self.existing_friendship_active.friendship_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(friendship.status, Friendship.FriendshipStatus.FRIENDSHIP_OVER)
        self.assertEqual(friendship.requesting_user, self.requesting_user)

    def test_update_friendship_status_from_over_to_any_except_requested_with_error_when_updated_by_requesting_user(
        self,
    ):
        for status in Friendship.FriendshipStatus:
            if status == Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED:
                continue
            request_data = {
                "status": status,
            }
            request = self.factory.patch(
                f"friendships/{self.existing_friendship_over.friendship_id}",
                request_data,
            )
            force_authenticate(request, self.friend_user)
            response = ModifyFriendshipView.as_view()(
                request, friendship_id=self.existing_friendship_over.friendship_id
            )
            error_message = "Votre amitié avec cette personne est terminée. Veuillez renvoyer une demande d'amitié."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_from_over_to_requested_by_requesting_user_with_success(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_over.friendship_id}", request_data
        )
        force_authenticate(request, self.friend_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_over.friendship_id
        )
        friendship = Friendship.objects.get(
            friendship_id=self.existing_friendship_over.friendship_id
        )
        resource_location = (
            f"/friendships/{self.existing_friendship_over.friendship_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(
            friendship.status, Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED
        )

    def test_update_friendship_status_from_over_to_any_with_error_when_updated_by_addressed_user(
        self,
    ):
        for status in Friendship.FriendshipStatus:
            request_data = {
                "status": status,
            }
            request = self.factory.patch(
                f"friendships/{self.existing_friendship_over.friendship_id}",
                request_data,
            )
            force_authenticate(request, self.addressed_user)
            response = ModifyFriendshipView.as_view()(
                request, friendship_id=self.existing_friendship_over.friendship_id
            )
            error_message = "Votre amitié avec cette personne est terminée. Vous ne pouvez pas en modifier le statut."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_from_requested_to_any_except_active_or_denied_with_error_when_updated_by_addressed_user(
        self,
    ):
        for status in Friendship.FriendshipStatus:
            if (
                status == Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE
                or status == Friendship.FriendshipStatus.FRIENDSHIP_DENIED
            ):
                continue
            request_data = {
                "status": status,
            }
            request = self.factory.patch(
                f"friendships/{self.existing_friendship_requested.friendship_id}",
                request_data,
            )
            force_authenticate(request, self.addressed_user)
            response = ModifyFriendshipView.as_view()(
                request, friendship_id=self.existing_friendship_requested.friendship_id
            )
            error_message = (
                "Vous ne pouvez qu'accepter ou refuser cette demande d'amitié."
            )
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_friendship_from_requested_to_active_with_success_when_addressed_user_accepts_request(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_requested.friendship_id}",
            request_data,
        )
        force_authenticate(request, self.addressed_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_requested.friendship_id
        )
        friendship = Friendship.objects.get(
            friendship_id=self.existing_friendship_requested.friendship_id
        )
        resource_location = (
            f"/friendships/{self.existing_friendship_requested.friendship_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(
            friendship.status, Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE
        )

    def test_update_friendship_from_requested_to_denied_with_success_when_addressed_user_denies_the_request(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_DENIED,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_requested.friendship_id}",
            request_data,
        )
        force_authenticate(request, self.addressed_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_requested.friendship_id
        )
        friendship = Friendship.objects.get(
            friendship_id=self.existing_friendship_requested.friendship_id
        )
        self.assertEqual(response.status_code, 204)
        self.assertEqual(
            friendship.status, Friendship.FriendshipStatus.FRIENDSHIP_DENIED
        )

    def test_update_friendship_status_from_denied_to_any_except_requested_with_error_when_updated_by_addressed_user(
        self,
    ):
        for status in Friendship.FriendshipStatus:
            if status == Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED:
                continue
            request_data = {
                "status": status,
            }
            request = self.factory.patch(
                f"friendships/{self.existing_friendship_denied.friendship_id}",
                request_data,
            )
            force_authenticate(request, self.denied_user)
            response = ModifyFriendshipView.as_view()(
                request, friendship_id=self.existing_friendship_denied.friendship_id
            )
            error_message = "Vous avez déjà refusé cette demande d'amitié. Veuillez en renvoyer une pour en modifier le statut."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_from_denied_to_requested_with_success_by_denying_user(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_denied.friendship_id}", request_data
        )
        force_authenticate(request, self.denied_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_denied.friendship_id
        )
        friendship = Friendship.objects.get(
            friendship_id=self.existing_friendship_denied.friendship_id
        )
        resource_location = (
            f"/friendships/{self.existing_friendship_denied.friendship_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(
            friendship.status, Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED
        )

    def test_update_friendship_status_from_denied_to_requested_by_denying_user_exchanges_requesting_and_addressed_user(
        self,
    ):
        request_data = {"status": Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED}
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_denied.friendship_id}", request_data
        )
        force_authenticate(request, self.denied_user)
        ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_denied.friendship_id
        )
        friendship = Friendship.objects.get(
            friendship_id=self.existing_friendship_denied.friendship_id
        )
        self.assertEqual(friendship.addressed_user, self.requesting_user)
        self.assertEqual(friendship.requesting_user, self.denied_user)

    def test_update_friendship_status_from_active_to_over_with_success_by_addressed_user_who_becomes_requesting_user(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_OVER,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_active.friendship_id}", request_data
        )
        force_authenticate(request, self.friend_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_active.friendship_id
        )
        friendship = Friendship.objects.get(
            friendship_id=self.existing_friendship_active.friendship_id
        )
        self.assertEqual(response.status_code, 204)
        self.assertEqual(friendship.status, Friendship.FriendshipStatus.FRIENDSHIP_OVER)
        self.assertEqual(friendship.requesting_user, self.friend_user)

    def test_update_friendship_status_from_active_to_requested_by_addressed_user_with_error(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_active.friendship_id}", request_data
        )
        force_authenticate(request, self.friend_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_active.friendship_id
        )
        error_message = "Il n'est pas possible de passer d'une amitié active à une demande d'amitié."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_from_active_to_denied_by_addressed_user_with_error(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_DENIED,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_active.friendship_id}", request_data
        )
        force_authenticate(request, self.friend_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_active.friendship_id
        )
        error_message = "Il n'est pas possible de passer d'une amitié active à refusée. Vous pouvez néanmoins mettre fin à cette amitié."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_from_active_to_active_by_addressed_user_with_error(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_active.friendship_id}", request_data
        )
        force_authenticate(request, self.friend_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_active.friendship_id
        )
        error_message = "Votre amitié avec cette personne est déjà active."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_update_friendship_status_with_error_if_invalid_status(
        self,
    ):
        request_data = {
            "status": "invalid status content",
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_active.friendship_id}", request_data
        )
        force_authenticate(request, self.friend_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_active.friendship_id
        )
        self.assertEqual(response.status_code, 400)

    def test_update_friendship_with_error_if_request_user_tries_to_change_second_user_id(
        self,
    ):
        request_data = {
            "status": Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
            "addressed_user": self.denied_user.user_id,
        }
        request = self.factory.patch(
            f"friendships/{self.existing_friendship_over.friendship_id}", request_data
        )
        force_authenticate(request, self.friend_user)
        response = ModifyFriendshipView.as_view()(
            request, friendship_id=self.existing_friendship_over.friendship_id
        )
        self.assertEqual(response.status_code, 403)


class GetUserFriendships(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.user_factory = api.factory.UserFactory()
        self.friendship_factory = api.factory.FriendshipFactory()

        self.requesting_user = self.user_factory.new()
        self.addressed_user = self.user_factory.new()
        self.friend_user = self.user_factory.new()
        self.denied_user = self.user_factory.new()
        self.external_user = User.objects.create(
            username="external", role=User.UserRole.EXTERNAL
        )

        self.existing_friendship_requested = self.friendship_factory.new(
            requesting_user=self.requesting_user,
            addressed_user=self.addressed_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        )
        self.existing_friendship_active = self.friendship_factory.new(
            requesting_user=self.requesting_user,
            addressed_user=self.friend_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )
        self.existing_friendship_denied = self.friendship_factory.new(
            requesting_user=self.requesting_user,
            addressed_user=self.denied_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_DENIED,
        )
        self.existing_external_friendship = self.friendship_factory.new(
            requesting_user=self.denied_user,
            addressed_user=self.external_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )

    def test_get_user_friendships_with_success_by_requesting_user(self):
        request = self.factory.get(f"users/{self.requesting_user.user_id}/friendships/")
        force_authenticate(request, self.requesting_user)
        response = GetUserFriendshipsView.as_view()(
            request, user_id=self.requesting_user.user_id
        )
        # Vérifie qu'on a bien une 200
        self.assertEqual(response.status_code, 200)
        # Vérifie qu'il y a bien 3 friendships
        self.assertEqual(len(response.data), 3)
        # Vérifie que les 3 friendships sont bien les bonnes
        self.assertEqual(
            response.data[0]["friendship_id"],
            str(self.existing_friendship_requested.friendship_id),
        )
        self.assertEqual(
            response.data[1]["friendship_id"],
            str(self.existing_friendship_active.friendship_id),
        )
        self.assertEqual(
            response.data[2]["friendship_id"],
            str(self.existing_friendship_denied.friendship_id),
        )

    def test_get_friendship_requested_with_success_by_addressed_user(self):
        request = self.factory.get(f"users/{self.addressed_user.user_id}/friendships/")
        force_authenticate(request, self.addressed_user)
        response = GetUserFriendshipsView.as_view()(
            request, user_id=self.addressed_user.user_id
        )
        # Vérifie qu'on a bien une 200
        self.assertEqual(response.status_code, 200)
        # Vérifie que c'est la bonne amitié
        self.assertEqual(
            response.data[0]["friendship_id"],
            str(self.existing_friendship_requested.friendship_id),
        )

    def test_get_friendship_denied_with_success_by_addressed_user(self):
        request = self.factory.get(f"users/{self.denied_user.user_id}/friendships/")
        force_authenticate(request, self.denied_user)
        response = GetUserFriendshipsView.as_view()(
            request, user_id=self.denied_user.user_id
        )
        # Vérifie qu'on a bien une 200
        self.assertEqual(response.status_code, 200)
        # Vérifie que c'est la bonne amitié
        self.assertEqual(
            response.data[0]["friendship_id"],
            str(self.existing_friendship_denied.friendship_id),
        )

    def test_get_friendship_active_with_success_by_addressed_user(self):
        request = self.factory.get(f"users/{self.friend_user.user_id}/friendships/")
        force_authenticate(request, self.friend_user)
        response = GetUserFriendshipsView.as_view()(
            request, user_id=self.friend_user.user_id
        )
        # Vérifie qu'on a bien une 200
        self.assertEqual(response.status_code, 200)
        # Vérifie que c'est la bonne amitié
        self.assertEqual(
            response.data[0]["friendship_id"],
            str(self.existing_friendship_active.friendship_id),
        )

    def test_get_user_friendships_with_error_if_user_has_no_friend(self):
        authenticated_user = self.user_factory.new()
        request = self.factory.get(f"users/{authenticated_user.user_id}/friendships/")
        force_authenticate(request, authenticated_user)
        response = GetUserFriendshipsView.as_view()(
            request, user_id=authenticated_user.user_id
        )
        # Vérifie qu'on a bien une 404
        self.assertEqual(response.status_code, 404)
        # Vérifie qu'on a le bon message d'erreur
        error_message = "Vous n'avez pas encore d'amitiés."
        self.assertEqual(response.data["error_message"], error_message)

    def test_get_user_friendships_with_error_if_requesting_user_is_different_than_queried_user_id(
        self,
    ):
        request = self.factory.get(f"users/{self.addressed_user.user_id}/friendships/")
        force_authenticate(request, self.requesting_user)
        response = GetUserFriendshipsView.as_view()(
            request, user_id=self.addressed_user.user_id
        )
        # Vérifie qu'on a bien une 403
        self.assertEqual(response.status_code, 403)

    def test_get_user_friendships_should_return_expected_data_by_addressed_user(
        self,
    ):
        request = self.factory.get(f"users/{self.denied_user.user_id}/friendships/")
        force_authenticate(request, self.denied_user)
        response = GetUserFriendshipsView.as_view()(
            request, user_id=self.denied_user.user_id
        )
        # Vérifie qu'on a bien toutes les données attendues
        expected_data = {
            "friendship_id": str(self.existing_friendship_denied.friendship_id),
            "status": self.existing_friendship_denied.status,
            "friend": {
                "is_requesting_user": True,
                "user_id": str(self.requesting_user.user_id),
                "username": self.requesting_user.username,
                "email": self.requesting_user.email,
                "role": self.requesting_user.role,
            },
        }
        self.assertEqual(response.data[0], expected_data)
        # Vérifie qu'on ne récupère pas le mot de passe de l'autre personne
        self.assertNotIn(
            "password",
            response.data[0]["friend"],
        )

    def test_get_user_friendships_should_return_expected_data_by_requesting_user(
        self,
    ):
        request = self.factory.get(f"users/{self.requesting_user.user_id}/friendships/")
        force_authenticate(request, self.requesting_user)
        response = GetUserFriendshipsView.as_view()(
            request, user_id=self.requesting_user.user_id
        )
        # Vérifie qu'on a bien toutes les données attendues
        expected_data = {
            "friendship_id": str(self.existing_friendship_active.friendship_id),
            "status": self.existing_friendship_active.status,
            "friend": {
                "is_requesting_user": False,
                "user_id": str(self.friend_user.user_id),
                "username": self.friend_user.username,
                "email": self.friend_user.email,
                "role": self.friend_user.role,
            },
        }
        self.assertEqual(response.data[1], expected_data)
        # Vérifie qu'on ne récupère pas le mot de passe de l'autre personne
        self.assertNotIn(
            "password",
            response.data[1]["friend"],
        )

    def test_get_user_friendships_should_return_expected_data_if_friend_is_external(
        self,
    ):
        request = self.factory.get(f"users/{self.denied_user.user_id}/friendships/")
        force_authenticate(request, self.denied_user)
        response = GetUserFriendshipsView.as_view()(
            request, user_id=self.denied_user.user_id
        )
        expected_data = {
            "friendship_id": str(self.existing_external_friendship.friendship_id),
            "status": self.existing_external_friendship.status,
            "friend": {
                "is_requesting_user": False,
                "user_id": str(self.external_user.user_id),
                "username": self.external_user.username,
                "role": self.external_user.role,
                "email": None,
            },
        }
        self.assertEqual(response.data[1], expected_data)


class GetUserSingleFriendship(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.user_factory = api.factory.UserFactory()
        self.friendship_factory = api.factory.FriendshipFactory()

        self.requesting_user = self.user_factory.new()
        self.addressed_user = self.user_factory.new()

        self.friend_status_requested = self.user_factory.new()
        self.friend_status_active = self.user_factory.new()
        self.friend_status_denied = self.user_factory.new()
        self.friend_status_over = self.user_factory.new()

        self.external_user = User.objects.create(
            username="external", role=User.UserRole.EXTERNAL
        )

        # Friendships with requesting_user
        self.r_friendship_requested = self.friendship_factory.new(
            requesting_user=self.requesting_user,
            addressed_user=self.friend_status_requested,
            status=Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        )
        self.r_friendship_active = self.friendship_factory.new(
            requesting_user=self.requesting_user,
            addressed_user=self.friend_status_active,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )
        self.r_friendship_denied = self.friendship_factory.new(
            requesting_user=self.requesting_user,
            addressed_user=self.friend_status_denied,
            status=Friendship.FriendshipStatus.FRIENDSHIP_DENIED,
        )
        self.r_friendship_over = self.friendship_factory.new(
            requesting_user=self.requesting_user,
            addressed_user=self.friend_status_over,
            status=Friendship.FriendshipStatus.FRIENDSHIP_OVER,
        )

        # Friendships with addressed_user
        self.a_friendship_requested = self.friendship_factory.new(
            requesting_user=self.friend_status_requested,
            addressed_user=self.addressed_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        )
        self.a_friendship_active = self.friendship_factory.new(
            requesting_user=self.friend_status_active,
            addressed_user=self.addressed_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )
        self.a_friendship_denied = self.friendship_factory.new(
            requesting_user=self.friend_status_denied,
            addressed_user=self.addressed_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_DENIED,
        )
        self.a_friendship_over = self.friendship_factory.new(
            requesting_user=self.friend_status_over,
            addressed_user=self.addressed_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_OVER,
        )
        self.external_friendship = self.friendship_factory.new(
            requesting_user=self.requesting_user,
            addressed_user=self.external_user,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )

    def test_get_user_friendship_with_success_by_requesting_user(self):
        request = self.factory.get(
            f"users/{self.requesting_user.user_id}/friendships/{self.r_friendship_active.friendship_id}"
        )
        force_authenticate(request, self.requesting_user)
        response = GetUserSingleFriendshipView.as_view()(
            request,
            friendship_id=self.r_friendship_active.friendship_id,
        )
        # Vérifie qu'on a bien une 200
        self.assertEqual(response.status_code, 200)
        # Vérifie que c'est la bonne
        self.assertEqual(
            response.data["friendship_id"], str(self.r_friendship_active.friendship_id)
        )

    def test_get_user_friendship_with_success_by_addressed_user(self):
        request = self.factory.get(
            f"users/{self.addressed_user.user_id}/friendships/{self.a_friendship_active.friendship_id}"
        )
        force_authenticate(request, self.addressed_user)
        response = GetUserSingleFriendshipView.as_view()(
            request,
            friendship_id=self.a_friendship_active.friendship_id,
        )
        # Vérifie qu'on a bien une 200
        self.assertEqual(response.status_code, 200)
        # Vérifie que c'est la bonne
        self.assertEqual(
            response.data["friendship_id"], str(self.a_friendship_active.friendship_id)
        )

    def test_get_user_friendship_with_error_when_user_does_not_request_one_of_his_friendships(
        self,
    ):
        request = self.factory.get(
            f"users/{self.addressed_user.user_id}/friendships/{self.a_friendship_active.friendship_id}"
        )
        force_authenticate(request, self.friend_status_over)
        response = GetUserSingleFriendshipView.as_view()(
            request,
            friendship_id=self.a_friendship_active.friendship_id,
        )
        # Vérifie qu'on a bien une 403
        self.assertEqual(response.status_code, 403)

    def test_get_user_friendship_with_error_when_friendship_does_not_exist(self):
        self.r_friendship_requested.delete()
        request = self.factory.get(
            f"users/{self.requesting_user.user_id}/friendships/{self.r_friendship_requested.friendship_id}"
        )
        force_authenticate(request, self.requesting_user)
        response = GetUserSingleFriendshipView.as_view()(
            request,
            friendship_id=self.r_friendship_requested.friendship_id,
        )
        # Vérifie qu'on a bien une 404
        self.assertEqual(response.status_code, 404)
        # Vérifie qu'on a le bon message d'erreur
        self.assertEqual(response.data["error_message"], "Cette amitié n'existe pas.")

    def test_get_user_friendship_should_return_expected_data(
        self,
    ):
        request = self.factory.get(
            f"users/{self.addressed_user.user_id}/friendships/{self.a_friendship_denied.friendship_id}"
        )
        force_authenticate(request, self.addressed_user)
        response = GetUserSingleFriendshipView.as_view()(
            request,
            friendship_id=self.a_friendship_denied.friendship_id,
        )
        # Vérifie qu'on a bien toutes les données attendues
        expected_data = {
            "friendship_id": str(self.a_friendship_denied.friendship_id),
            "status": self.a_friendship_denied.status,
            "friend": {
                "is_requesting_user": True,
                "user_id": str(self.friend_status_denied.user_id),
                "username": self.friend_status_denied.username,
                "email": self.friend_status_denied.email,
                "role": self.friend_status_denied.role,
            },
        }
        self.assertEqual(response.data, expected_data)
