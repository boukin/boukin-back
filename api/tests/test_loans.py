import api.factory
from random import randint
from datetime import date, datetime, timedelta
from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate
from api.views import (
    CreateLoanByBookOwnerView,
    RequestLoanByFriendView,
    UpdateLoanView,
    GetAllLoansView,
)
from api.models import User, Book, OwnedBook, Loan, Friendship


class AddLoanTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.today = date.today()
        self.loaned_book = Book.objects.create(
            title="titre", author_fname="John", author_lname="Doe", isbn="9781237894561"
        )
        self.book_owner = User.objects.create(
            username="book_owner", email="book_owner@boukin.org", password="b00k_Own3r"
        )
        self.book_borrower = User.objects.create(
            username="book_borrower",
            email="book_borrower@boukin.org",
            password="b00k_Borrow3r",
        )
        self.another_user = User.objects.create(
            username="another_user",
            email="another_user@boukin.org",
            password="Another7_U53r",
        )
        self.ownedbook = OwnedBook.objects.create(
            owner=self.book_owner, book=self.loaned_book
        )
        self.loan_data = {
            "owner": self.book_owner.user_id,
            "borrower": self.book_borrower.user_id,
            "owned_book": self.ownedbook.owned_book_id,
            "starting_date": self.today,
        }

    def test_post_loan_with_success(self):
        request = self.factory.post("loans/", self.loan_data)
        force_authenticate(request, self.book_owner)
        response = CreateLoanByBookOwnerView.as_view()(request)
        updated_owned_book = OwnedBook.objects.get(
            owned_book_id=self.ownedbook.owned_book_id
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["status"], Loan.LoanStatus.LOAN_ACTIVE)
        self.assertEqual(updated_owned_book.status, OwnedBook.OwnedBookStatus.LOANED)

    def test_post_loan_with_error_if_owner_is_same_as_borrower(self):
        self.loan_data.update({"borrower": self.book_owner.user_id})
        request = self.factory.post("loans/", self.loan_data)
        force_authenticate(request, self.book_owner)
        response = CreateLoanByBookOwnerView.as_view()(request)
        error_message = "Vous ne pouvez pas vous prêter un livre."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_post_loan_with_error_if_not_book_owner(self):
        request = self.factory.post("loans/", self.loan_data)
        force_authenticate(request, self.another_user)
        response = CreateLoanByBookOwnerView.as_view()(request)
        self.assertEqual(response.status_code, 403)

    def test_post_with_error_if_starting_date_is_later_than_ending_date(self):
        ending_date = (datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d")
        self.loan_data.update({"ending_date": ending_date})
        request = self.factory.post("loans/", self.loan_data)
        force_authenticate(request, self.book_owner)
        response = CreateLoanByBookOwnerView.as_view()(request)
        error_message = (
            "La date de début d'emprunt ne peut être après celle de fin d'emprunt."
        )
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_post_with_error_if_owned_book_status_is_loaned(self):
        OwnedBook.objects.filter(owned_book_id=self.ownedbook.owned_book_id).update(
            status=OwnedBook.OwnedBookStatus.LOANED
        )
        request = self.factory.post("loans/", self.loan_data)
        force_authenticate(request, self.book_owner)
        response = CreateLoanByBookOwnerView.as_view()(request)
        error_message = "Vous ne pouvez pas emprunter un livre en cours de prêt."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_post_with_error_if_owned_book_query_fails(self):
        self.ownedbook.delete()
        request = self.factory.post("loans/", self.loan_data)
        force_authenticate(request, self.book_owner)
        response = CreateLoanByBookOwnerView.as_view()(request)
        self.assertEqual(response.status_code, 404)


class RequestLoanTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.requested_book = Book.objects.create(
            title="titre", author_fname="John", author_lname="Doe", isbn="9781237894561"
        )
        self.book_owner = User.objects.create(
            username="book_owner", email="book_owner@boukin.org", password="b00k_Own3r"
        )
        self.ownedbook = OwnedBook.objects.create(
            owner=self.book_owner, book=self.requested_book
        )
        self.book_borrower = User.objects.create(
            username="book_borrower",
            email="book_borrower@boukin.org",
            password="b00k_Borrow3r",
        )
        self.friendship = Friendship.objects.create(
            requesting_user=self.book_owner,
            addressed_user=self.book_borrower,
            status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
        )
        self.loan_data = {
            "owner": self.book_owner.user_id,
            "borrower": self.book_borrower.user_id,
            "owned_book": self.ownedbook.owned_book_id,
        }

    def test_request_loan_with_success(self):
        request = self.factory.post("loans/request/", self.loan_data)
        force_authenticate(request, self.book_borrower)
        response = RequestLoanByFriendView.as_view()(request)
        not_updated_owned_book = OwnedBook.objects.get(
            owned_book_id=self.ownedbook.owned_book_id
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["status"], Loan.LoanStatus.LOAN_REQUESTED)
        self.assertEqual(not_updated_owned_book.status, self.ownedbook.status)

    def test_request_with_error_if_not_friend_with_owner(self):
        # test all the friendship status different from "active"
        for status in [
            Friendship.FriendshipStatus.FRIENDSHIP_DENIED,
            Friendship.FriendshipStatus.FRIENDSHIP_OVER,
            Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
        ]:
            Friendship.objects.filter(
                friendship_id=self.friendship.friendship_id
            ).update(status=status)
            request = self.factory.post("loans/request/", self.loan_data)
            force_authenticate(request, self.book_borrower)
            response = RequestLoanByFriendView.as_view()(request)
            self.assertEqual(response.status_code, 403)

        # test when there is no friendship entry at all
        Friendship.objects.filter(friendship_id=self.friendship.friendship_id).delete()
        request = self.factory.post("loans/request/", self.loan_data)
        force_authenticate(request, self.book_borrower)
        response = RequestLoanByFriendView.as_view()(request)
        self.assertEqual(response.status_code, 403)

    def test_request_with_error_if_already_requested_by_the_same_user(self):
        Loan.objects.create(
            owner=self.book_owner,
            borrower=self.book_borrower,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.LOAN_REQUESTED,
        )
        request = self.factory.post("loans/request/", self.loan_data)
        force_authenticate(request, self.book_borrower)
        # try to request a loan already requested
        response = RequestLoanByFriendView.as_view()(request)
        error_message = "Vous avez déjà une demande d'emprunt en cours pour ce livre."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_request_with_error_if_requested_book_is_loaned(self):

        OwnedBook.objects.filter(owned_book_id=self.ownedbook.owned_book_id).update(
            status=OwnedBook.OwnedBookStatus.LOANED
        )
        request = self.factory.post("loans/request/", self.loan_data)
        force_authenticate(request, self.book_borrower)
        response = RequestLoanByFriendView.as_view()(request)
        error_message = "Vous ne pouvez pas emprunter un livre en cours de prêt."
        self.assertEqual(response.status_code, 400)
        self.assertIn(error_message, str(response.data))

    def test_request_with_error_if_owned_book_request_fails(self):

        self.ownedbook.delete()
        request = self.factory.post("loans/request/", self.loan_data)
        force_authenticate(request, self.book_borrower)
        response = RequestLoanByFriendView.as_view()(request)
        self.assertEqual(response.status_code, 404)


class UpdateLoanTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.requested_book = Book.objects.create(
            title="titre", author_fname="John", author_lname="Doe", isbn="9781237894561"
        )
        self.another_user = User.objects.create(
            username="another_user",
            email="another_user@boukin.org",
            password="anoTh3r@us3r",
        )
        self.book_owner = User.objects.create(
            username="book_owner", email="book_owner@boukin.org", password="b00k_Own3r"
        )
        self.ownedbook = OwnedBook.objects.create(
            owner=self.book_owner, book=self.requested_book
        )
        self.book_borrower = User.objects.create(
            username="book_borrower",
            email="book_borrower@boukin.org",
            password="b00k_Borrow3r",
        )
        self.loan_denied = Loan.objects.create(
            owner=self.book_owner,
            borrower=self.book_borrower,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.LOAN_DENIED,
        )
        self.loan_requested = Loan.objects.create(
            owner=self.book_owner,
            borrower=self.book_borrower,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.LOAN_REQUESTED,
        )
        self.loan_active = Loan.objects.create(
            owner=self.book_owner,
            borrower=self.book_borrower,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.LOAN_ACTIVE,
        )
        self.return_requested = Loan.objects.create(
            owner=self.book_owner,
            borrower=self.book_borrower,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.RETURN_REQUESTED,
        )
        self.return_confirmation_pending = Loan.objects.create(
            owner=self.book_owner,
            borrower=self.book_borrower,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.RETURN_CONFIRMATION_PENDING,
        )
        self.return_confirmed = Loan.objects.create(
            owner=self.book_owner,
            borrower=self.book_borrower,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.RETURN_CONFIRMED,
        )
        self.loan_update_data = {
            "status": Loan.LoanStatus.LOAN_ACTIVE,
        }
        self.today = date.today()
        self.one_week_later = self.today + timedelta(days=7)
        self.loan_update_data_with_dates = {
            "status": Loan.LoanStatus.LOAN_ACTIVE,
            "starting_date": self.today,
            "ending_date": self.one_week_later,
        }
        self.loan_update_with_dates_no_status = {
            "starting_date": self.today,
            "ending_date": self.one_week_later,
        }

    def test_update_loan_status_with_error_if_requested_loan_not_found(self):
        self.loan_requested.delete()
        request = self.factory.patch(
            f"loans/{self.loan_requested.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.book_owner)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.loan_requested.loan_id
        )
        self.assertEqual(response.status_code, 404)

    def test_update_loan_status_with_error_if_request_body_has_a_field_other_than_status_starting_and_ending_dates(
        self,
    ):
        self.loan_update_data_with_dates.update({"invalid_field": "invalid_value"})
        request = self.factory.patch(
            f"loans/{self.loan_requested.loan_id}", self.loan_update_data_with_dates
        )
        force_authenticate(request, self.book_owner)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.loan_requested.loan_id
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data["message"], "Vous ne pouvez modifier que le statut du prêt"
        )

    def test_update_loan_status_with_error_if_not_involved_in_loan(self):
        request = self.factory.patch(
            f"loans/{self.loan_requested.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.another_user)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.loan_requested.loan_id
        )
        self.assertEqual(
            response.data["detail"],
            "You do not have permission to perform this action.",
        )
        self.assertEqual(response.status_code, 403)

    def test_update_loan_status_by_book_owner_with_success_from_loan_requested_to_loan_active(
        self,
    ):
        request = self.factory.patch(
            f"loans/{self.loan_requested.loan_id}", self.loan_update_data_with_dates
        )
        force_authenticate(request, self.book_owner)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.loan_requested.loan_id
        )
        query_loan = Loan.objects.get(loan_id=self.loan_requested.loan_id)
        resource_location = (
            f"/users/{self.book_owner.user_id}/loans/{self.loan_requested.loan_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(query_loan.status, Loan.LoanStatus.LOAN_ACTIVE)
        self.assertEqual(query_loan.starting_date, self.today)
        self.assertEqual(query_loan.ending_date, self.one_week_later)

    def test_update_loan_status_by_borrower_with_error_if_attempting_to_update_loan_starting_and_ending_dates(
        self,
    ):
        request = self.factory.patch(
            f"loans/{self.loan_requested.loan_id}",
            self.loan_update_with_dates_no_status,
        )
        force_authenticate(request, self.book_borrower)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.loan_requested.loan_id
        )
        self.assertEqual(response.status_code, 400)
        error_message = "Vous ne pouvez modifier les dates de début ou de fin de prêt."
        self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_borrower_with_error_if_attempting_to_update_status_and_dates(
        self,
    ):
        request = self.factory.patch(
            f"loans/{self.loan_requested.loan_id}",
            self.loan_update_data_with_dates,
        )
        force_authenticate(request, self.book_borrower)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.loan_requested.loan_id
        )
        self.assertEqual(response.status_code, 400)
        status_error_message = (
            "Vous avez demandé ce prêt, vous ne pouvez en modifier le statut."
        )
        dates_error_message = (
            "Vous ne pouvez modifier les dates de début ou de fin de prêt."
        )
        self.assertIn(status_error_message, str(response.data))
        self.assertIn(dates_error_message, str(response.data))

    def test_update_loan_status_by_book_owner_with_error_if_invalid_data_for_status(
        self,
    ):
        self.loan_update_data.update({"status": "invalid_status"})
        request = self.factory.patch(
            f"loans/{self.loan_requested.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.book_owner)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.loan_requested.loan_id
        )
        self.assertEqual(response.status_code, 400)

    def test_update_loan_with_dates_by_owner_with_error_from_any_status_other_than_from_requested_to_active(
        self,
    ):
        loans = [
            self.loan_active,
            self.loan_denied,
            self.return_requested,
            self.return_confirmation_pending,
            self.return_confirmed,
        ]
        for status in Loan.LoanStatus:
            if status == Loan.LoanStatus.LOAN_ACTIVE:
                continue
            self.loan_update_data_with_dates.update({"status": status})
            for loan in loans:
                request = self.factory.patch(
                    f"loans/{loan.loan_id}",
                    self.loan_update_data_with_dates,
                )
                force_authenticate(request, self.book_owner)
                response = UpdateLoanView.as_view()(request, loan_id=loan.loan_id)
                self.assertEqual(response.status_code, 400)
                dates_error_message = (
                    "Vous ne pouvez modifier les dates du prêt si vous l'avez accepté."
                )
                self.assertIn(dates_error_message, str(response.data))

    def test_update_loan_status_by_book_owner_with_success_from_loan_requested_to_loan_denied(
        self,
    ):
        self.loan_update_data.update({"status": Loan.LoanStatus.LOAN_DENIED})
        request = self.factory.patch(
            f"loans/{self.loan_requested.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.book_owner)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.loan_requested.loan_id
        )
        query_loan = Loan.objects.get(loan_id=self.loan_requested.loan_id)
        resource_location = (
            f"/users/{self.book_owner.user_id}/loans/{self.loan_requested.loan_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(query_loan.status, Loan.LoanStatus.LOAN_DENIED)

    def test_update_loan_status_by_book_owner_with_success_from_loan_active_to_return_requested(
        self,
    ):
        self.loan_update_data.update(
            {
                "status": Loan.LoanStatus.RETURN_REQUESTED,
            }
        )
        request = self.factory.patch(
            f"loans/{self.loan_active.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.book_owner)
        response = UpdateLoanView.as_view()(request, loan_id=self.loan_active.loan_id)
        query_loan = Loan.objects.get(loan_id=self.loan_active.loan_id)
        resource_location = (
            f"/users/{self.book_owner.user_id}/loans/{self.loan_active.loan_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(query_loan.status, Loan.LoanStatus.RETURN_REQUESTED)

    def test_update_loan_status_by_book_owner_with_success_from_loan_active_to_return_confirmed(
        self,
    ):
        self.loan_update_data.update(
            {
                "status": Loan.LoanStatus.RETURN_CONFIRMED,
            }
        )
        request = self.factory.patch(
            f"loans/{self.loan_active.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.book_owner)
        response = UpdateLoanView.as_view()(request, loan_id=self.loan_active.loan_id)
        query_loan = Loan.objects.get(loan_id=self.loan_active.loan_id)
        resource_location = (
            f"/users/{self.book_owner.user_id}/loans/{self.loan_active.loan_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(query_loan.status, Loan.LoanStatus.RETURN_CONFIRMED)

    def test_update_loan_status_by_book_owner_with_success_from_return_requested_to_return_confirmed(
        self,
    ):
        self.loan_update_data.update(
            {
                "status": Loan.LoanStatus.RETURN_CONFIRMED,
            }
        )
        request = self.factory.patch(
            f"loans/{self.return_requested.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.book_owner)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.return_requested.loan_id
        )
        query_loan = Loan.objects.get(loan_id=self.return_requested.loan_id)
        resource_location = (
            f"/users/{self.book_owner.user_id}/loans/{self.return_requested.loan_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(query_loan.status, Loan.LoanStatus.RETURN_CONFIRMED)

    def test_update_loan_status_by_book_owner_with_success_from_return_confirmation_pending_to_return_confirmed(
        self,
    ):
        self.loan_update_data.update(
            {
                "status": Loan.LoanStatus.RETURN_CONFIRMED,
            }
        )
        request = self.factory.patch(
            f"loans/{self.return_confirmation_pending.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.book_owner)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.return_confirmation_pending.loan_id
        )
        query_loan = Loan.objects.get(loan_id=self.return_confirmation_pending.loan_id)
        resource_location = f"/users/{self.book_owner.user_id}/loans/{self.return_confirmation_pending.loan_id}/"
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(query_loan.status, Loan.LoanStatus.RETURN_CONFIRMED)

    def test_update_loan_status_by_book_owner_with_success_from_loan_denied_to_loan_active(
        self,
    ):
        # Le-a propriétaire d'un livre peut autoriser le prêt d'un livre auparavant refusé.

        self.loan_update_data.update({"status": Loan.LoanStatus.LOAN_ACTIVE})
        request = self.factory.patch(
            f"loans/{self.loan_denied.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.book_owner)
        response = UpdateLoanView.as_view()(request, loan_id=self.loan_denied.loan_id)
        query_loan = Loan.objects.get(loan_id=self.loan_denied.loan_id)
        resource_location = (
            f"/users/{self.book_owner.user_id}/loans/{self.loan_denied.loan_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(query_loan.status, Loan.LoanStatus.LOAN_ACTIVE)

    def test_update_loan_status_by_book_owner_with_error_from_loan_requested_to_any_return_status(
        self,
    ):

        for status in Loan.LoanStatus:
            if (
                status == Loan.LoanStatus.LOAN_DENIED
                or status == Loan.LoanStatus.LOAN_ACTIVE
            ):
                continue
            self.loan_update_data.update(
                {
                    "status": status,
                }
            )
            request = self.factory.patch(
                f"loans/{self.loan_requested.loan_id}", self.loan_update_data
            )
            force_authenticate(request, self.book_owner)
            response = UpdateLoanView.as_view()(
                request, loan_id=self.loan_requested.loan_id
            )
            error_message = "Vous devez accepter ou refuser cette demande de prêt."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_owner_with_error_from_loan_active_to_any_other_status_other_than_return_requested_or_confirmed(
        self,
    ):
        for status in Loan.LoanStatus:
            if (
                status == Loan.LoanStatus.RETURN_REQUESTED
                or status == Loan.LoanStatus.RETURN_CONFIRMED
            ):
                continue
            self.loan_update_data.update(
                {
                    "status": status,
                }
            )
            request = self.factory.patch(
                f"loans/{self.loan_active.loan_id}", self.loan_update_data
            )
            force_authenticate(request, self.book_owner)
            response = UpdateLoanView.as_view()(
                request, loan_id=self.loan_active.loan_id
            )
            error_message = "Un livre en cours de prêt ne peut être demandé en prêt."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_owner_with_error_from_return_confirmed_to_any_other_status(
        self,
    ):

        for status in Loan.LoanStatus:
            self.loan_update_data.update(
                {
                    "status": status,
                }
            )
            request = self.factory.patch(
                f"loans/{self.return_confirmed.loan_id}", self.loan_update_data
            )
            force_authenticate(request, self.book_owner)
            response = UpdateLoanView.as_view()(
                request, loan_id=self.return_confirmed.loan_id
            )
            error_message = "Vous avez confirmé le retour de ce livre, vous ne pouvez modifier le statut de prêt."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_owner_with_error_from_loan_denied_to_any_other_status_other_than_loan_active(
        self,
    ):

        for status in Loan.LoanStatus:
            if status == Loan.LoanStatus.LOAN_ACTIVE:
                continue
            self.loan_update_data.update(
                {
                    "status": status,
                }
            )
            request = self.factory.patch(
                f"loans/{self.loan_denied.loan_id}", self.loan_update_data
            )
            force_authenticate(request, self.book_owner)
            response = UpdateLoanView.as_view()(
                request, loan_id=self.loan_denied.loan_id
            )
            error_message = "Vous avez refusé de prêter ce livre, vous ne pouvez modifier le statut de prêt, sauf si vous voulez autoriser le prêt."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_owner_with_error_from_return_requested_to_any_other_status_other_than_return_confirmed(
        self,
    ):

        for status in Loan.LoanStatus:
            if status == Loan.LoanStatus.RETURN_CONFIRMED:
                continue
            self.loan_update_data.update(
                {
                    # "loan_id": self.return_requested.loan_id,
                    "status": status,
                }
            )
            request = self.factory.patch(
                f"loans/{self.return_requested.loan_id}", self.loan_update_data
            )
            force_authenticate(request, self.book_owner)
            response = UpdateLoanView.as_view()(
                request, loan_id=self.return_requested.loan_id
            )
            error_message = "Vous avez demandé le retour de ce livre, vous ne pouvez modifier le statut de prêt, sauf si vous voulez confirmer le retour."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_owner_with_error_from_return_confirmation_pending_to_any_other_status_other_than_return_confirmed(
        self,
    ):

        for status in Loan.LoanStatus:
            if status == Loan.LoanStatus.RETURN_CONFIRMED:
                continue
            self.loan_update_data.update(
                {
                    # "loan_id": self.return_confirmation_pending.loan_id,
                    "status": status,
                }
            )
            request = self.factory.patch(
                f"loans/{self.return_confirmation_pending.loan_id}",
                self.loan_update_data,
            )
            force_authenticate(request, self.book_owner)
            response = UpdateLoanView.as_view()(
                request, loan_id=self.return_confirmation_pending.loan_id
            )
            error_message = "Votre confirmation de retour de prêt a été demandée, vous ne pouvez modifier le statut de prêt, sauf si vous voulez confirmer le retour."
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_borrower_with_success_from_loan_active_to_return_confirmation_pending(
        self,
    ):
        self.loan_update_data.update(
            {
                "status": Loan.LoanStatus.RETURN_CONFIRMATION_PENDING,
            }
        )
        request = self.factory.patch(
            f"loans/{self.loan_active.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.book_borrower)
        response = UpdateLoanView.as_view()(request, loan_id=self.loan_active.loan_id)
        query_loan = Loan.objects.get(loan_id=self.loan_active.loan_id)
        resource_location = (
            f"/users/{self.book_borrower.user_id}/loans/{self.loan_active.loan_id}/"
        )
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(query_loan.status, Loan.LoanStatus.RETURN_CONFIRMATION_PENDING)

    def test_update_loan_status_by_book_borrower_with_success_from_return_requested_to_return_confirmation_pending(
        self,
    ):

        self.loan_update_data.update(
            {
                "status": Loan.LoanStatus.RETURN_CONFIRMATION_PENDING,
            }
        )
        request = self.factory.patch(
            f"loans/{self.return_requested.loan_id}", self.loan_update_data
        )
        force_authenticate(request, self.book_borrower)
        response = UpdateLoanView.as_view()(
            request, loan_id=self.return_requested.loan_id
        )
        query_loan = Loan.objects.get(loan_id=self.return_requested.loan_id)
        resource_location = f"/users/{self.book_borrower.user_id}/loans/{self.return_requested.loan_id}/"
        self.assertEqual(response["Content-Location"], resource_location)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(query_loan.status, Loan.LoanStatus.RETURN_CONFIRMATION_PENDING)

    def test_update_loan_status_by_book_borrower_with_error_from_loan_active_to_any_other_status_except_confirmation_pending(
        self,
    ):

        for status in Loan.LoanStatus:
            if status == Loan.LoanStatus.RETURN_CONFIRMATION_PENDING:
                continue
            self.loan_update_data.update({"status": status})
            request = self.factory.patch(
                f"loans/{self.loan_active.loan_id}", self.loan_update_data
            )
            force_authenticate(request, self.book_borrower)
            error_message = "Ce prêt est en cours, vous ne pouvez en modifier le statut, sauf si vous rendez le livre prêté."
            response = UpdateLoanView.as_view()(
                request, loan_id=self.loan_active.loan_id
            )
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_borrower_with_error_from_loan_denied_to_any_other_status(
        self,
    ):
        for status in Loan.LoanStatus:
            self.loan_update_data.update({"status": status})
            request = self.factory.patch(
                f"loans/{self.loan_denied.loan_id}", self.loan_update_data
            )
            force_authenticate(request, self.book_borrower)
            error_message = "Ce prêt vous a été refusé, vous ne pouvez le modifier."
            response = UpdateLoanView.as_view()(
                request, loan_id=self.loan_denied.loan_id
            )
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_borrower_with_error_from_loan_requested_to_any_other_status(
        self,
    ):

        for status in Loan.LoanStatus:
            self.loan_update_data.update({"status": status})
            request = self.factory.patch(
                f"loans/{self.loan_requested.loan_id}", self.loan_update_data
            )
            force_authenticate(request, self.book_borrower)
            error_message = (
                "Vous avez demandé ce prêt, vous ne pouvez en modifier le statut."
            )
            response = UpdateLoanView.as_view()(
                request, loan_id=self.loan_requested.loan_id
            )
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_borrower_with_error_from_return_requested_to_any_other_status_except_confirmation_pending(
        self,
    ):

        for status in Loan.LoanStatus:
            if status == Loan.LoanStatus.RETURN_CONFIRMATION_PENDING:
                continue
            self.loan_update_data.update({"status": status})
            request = self.factory.patch(
                f"loans/{self.return_requested.loan_id}", self.loan_update_data
            )
            force_authenticate(request, self.book_borrower)
            error_message = "Vous avez une demande de retour en cours, vous ne pouvez en modifier le statut, sauf si vous rendez le livre prêté."
            response = UpdateLoanView.as_view()(
                request, loan_id=self.return_requested.loan_id
            )
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_borrower_with_error_from_return_confirmation_pending_to_any_other_status(
        self,
    ):

        for status in Loan.LoanStatus:

            self.loan_update_data.update({"status": status})
            request = self.factory.patch(
                f"loans/{self.return_confirmation_pending.loan_id}",
                self.loan_update_data,
            )
            force_authenticate(request, self.book_borrower)
            error_message = "Votre retour de prêt a bien été enregistré, il est en attente de confirmation. Vous ne pouvez en modifier le statut."
            response = UpdateLoanView.as_view()(
                request, loan_id=self.return_confirmation_pending.loan_id
            )
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))

    def test_update_loan_status_by_book_borrower_with_error_from_return_confirmed_to_any_other_status(
        self,
    ):

        for status in Loan.LoanStatus:

            self.loan_update_data.update({"status": status})
            request = self.factory.patch(
                f"loans/{self.return_confirmed.loan_id}",
                self.loan_update_data,
            )
            force_authenticate(request, self.book_borrower)
            error_message = "Votre retour de prêt a bien été confirmé. Vous ne pouvez en modifier le statut."
            response = UpdateLoanView.as_view()(
                request, loan_id=self.return_confirmed.loan_id
            )
            self.assertEqual(response.status_code, 400)
            self.assertIn(error_message, str(response.data))


class GetLoansTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.today = date.today()
        self.loaned_book = Book.objects.create(
            title="titre", author_fname="John", author_lname="Doe", isbn="9781237894561"
        )
        self.book_owner = User.objects.create(
            username="book_owner", email="book_owner@boukin.org", password="b00k_Own3r"
        )
        self.book_borrower = User.objects.create(
            username="book_borrower",
            email="book_borrower@boukin.org",
            password="b00k_Borrow3r",
        )
        self.another_user = User.objects.create(
            username="another_user",
            email="another_user@boukin.org",
            password="Another7_U53r",
        )
        self.ownedbook = OwnedBook.objects.create(
            owner=self.book_owner, book=self.loaned_book
        )
        self.loan_active = Loan.objects.create(
            owner=self.book_owner,
            borrower=self.book_borrower,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.LOAN_ACTIVE,
        )
        self.loan_denied = Loan.objects.create(
            owner=self.book_owner,
            borrower=self.book_borrower,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.LOAN_DENIED,
        )
        self.loan_requested = Loan.objects.create(
            owner=self.book_owner,
            borrower=self.book_borrower,
            owned_book=self.ownedbook,
            status=Loan.LoanStatus.LOAN_REQUESTED,
        )

    def test_get_all_loans_with_success_by_user_requesting_own_loans(self):
        request = self.factory.get(
            f"users/{self.loan_active.owner.user_id}/loans/",
        )
        force_authenticate(request, self.book_owner)
        response = GetAllLoansView.as_view()(
            request, user_id=self.loan_active.owner.user_id
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["loans"]), 3)
        self.assertEqual(
            response.data["loans"][0]["loan_id"],
            str(self.loan_active.loan_id),
        )
        self.assertEqual(
            response.data["loans"][0]["borrower"]["username"],
            self.book_borrower.username,
        )
        self.assertEqual(
            response.data["loans"][0]["owned_book"]["book"]["title"],
            self.loaned_book.title,
        )
        self.assertEqual(
            response.data["loans"][0]["status"], Loan.LoanStatus.LOAN_ACTIVE
        )

    def test_get_all_loans_with_error_if_there_are_no_ongoing_loans(self):
        self.loan_active.delete()
        self.loan_denied.delete()
        self.loan_requested.delete()
        request = self.factory.get(
            f"users/{self.loan_active.owner.user_id}/loans/",
        )
        error_message = "Vous n'avez pas de prêts en cours"
        force_authenticate(request, self.book_owner)
        response = GetAllLoansView.as_view()(
            request, user_id=self.loan_active.owner.user_id
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data["error_message"], error_message)

    def test_get_all_loans_with_error_if_no_uuid_is_passed_to_the_request(self):
        request = self.factory.get("loans/")
        force_authenticate(request, self.book_owner)
        response = GetAllLoansView.as_view()(request, user_id=None)

        self.assertEqual(response.status_code, 400)

    def test_get_all_loans_with_error_if_requesting_user_id_is_different_from_queried_user_id(
        self,
    ):
        request = self.factory.get(f"users/{self.loan_active.owner.user_id}/loans/")
        force_authenticate(request, self.book_borrower)
        response = GetAllLoansView.as_view()(
            request, user_id=self.loan_active.owner.user_id
        )
        self.assertEqual(response.status_code, 403)

    def test_get_all_loans_should_only_return_owner_and_borrower_username_and_id(self):
        request = self.factory.get(
            f"users/{self.loan_active.owner.user_id}/loans/",
        )
        force_authenticate(request, self.book_owner)
        response = GetAllLoansView.as_view()(
            request, user_id=self.loan_active.owner.user_id
        )

        available_fields = ["username", "user_id"]
        unavailable_fields = [
            "email",
            "password",
            "last_login",
            "role",
            "created_at",
            "updated_at",
        ]
        self.assertEqual(response.status_code, 200)
        for index, loan in enumerate(response.data):
            for field in available_fields:
                self.assertIn(field, response.data["loans"][index]["borrower"])
                self.assertIn(field, response.data["loans"][index]["owner"])
            for field in unavailable_fields:
                self.assertNotIn(field, response.data["loans"][index]["borrower"])
                self.assertNotIn(field, response.data["loans"][index]["owner"])

    def test_get_all_loans_returns_total_number_of_loans(self):
        loan_factory = api.factory.LoanFactory()
        book_factory = api.factory.BookFactory()
        owned_book_factory = api.factory.OwnedBookFactory()
        total_number_of_loans = randint(11, 40)
        for x in range(total_number_of_loans):
            book = book_factory.new()
            owned_book = owned_book_factory.new(owner=self.another_user, book=book)
            loan_factory.new(
                owner=self.another_user,
                borrower=self.book_borrower,
                owned_book=owned_book,
            )

        request = self.factory.get(
            f"users/{self.another_user.user_id}/loans/",
        )
        force_authenticate(request, self.another_user)
        response = GetAllLoansView.as_view()(request, user_id=self.another_user.user_id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["total_number_of_loans"], total_number_of_loans)

    def test_get_all_loans_returns_loans_where_user_is_owner_and_borrower(self):
        loan_factory = api.factory.LoanFactory()
        book_factory = api.factory.BookFactory()
        owned_book_factory = api.factory.OwnedBookFactory()
        user_factory = api.factory.UserFactory()
        requesting_user = user_factory.new()
        another_user = user_factory.new()

        for x in range(5):
            book = book_factory.new()
            another_book = book_factory.new()
            book_owned_by_requesting_user = owned_book_factory.new(
                owner=requesting_user, book=book
            )
            book_owned_by_another_user = owned_book_factory.new(
                owner=another_user, book=another_book
            )
            loan_factory.new(
                owner=requesting_user,
                borrower=another_user,
                owned_book=book_owned_by_requesting_user,
            )
            loan_factory.new(
                owner=another_user,
                borrower=requesting_user,
                owned_book=book_owned_by_another_user,
            )

        request = self.factory.get(
            f"users/{requesting_user.user_id}/loans/",
        )
        force_authenticate(request, requesting_user)
        response = GetAllLoansView.as_view()(request, user_id=requesting_user.user_id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["total_number_of_loans"], 10)
