from django.core.mail import send_mail
from django.forms import ValidationError
from django.http.response import HttpResponseRedirect
from django.template.loader import render_to_string
from rest_framework.renderers import JSONRenderer
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from drf_spectacular.utils import extend_schema, OpenApiParameter
from drf_spectacular.types import OpenApiTypes
from smtplib import SMTPException
from .models import OwnedBook, User, Friendship, Loan, Book
from .serializers import (
    ExternalUserSerializer,
    UpdateLoanSerializer,
    UserSerializer,
    GetUserSerializer,
    BookSerializer,
    CreateOwnedBookSerializer,
    ListOwnedBooksSerializer,
    UpdateOwnedBookStatusSerializer,
    CreateLoanSerializer,
    GetAllLoansSerializer,
    FriendshipSerializer,
    RequestLoanSerializer,
    GenerateTokenSerializer,
    UpdateFriendshipSerializer,
    GetUserFriendshipsSerializer,
    UpdateUserSerializer,
)
from .permissions import (
    IsBookOwnerOrReadOnly,
    IsRequestUser,
    IsLoanOwnerOrBorrower,
    IsAFriend,
    IsFriendshipMember,
    IsStatusUpdateOnly,
)


class LoginUserView(ObtainAuthToken):
    permission_classes = [permissions.AllowAny]
    renderer_classes = [JSONRenderer]
    serializer_class = GenerateTokenSerializer

    def post(self, request, format=None):

        serializer = GenerateTokenSerializer(
            data=request.data, context={"request": request}
        )
        if serializer.is_valid():
            email = serializer.validated_data["email"]
            user = User.objects.get(email=email)
            try:
                token = Token.objects.get(user=user)
            except Token.DoesNotExist:
                token = Token.objects.create(user=user)
            finally:
                token_data = {
                    "token": token.key,
                    "user_id": user.user_id,
                    "email": user.email,
                    "username": user.username,
                }
            return Response(token_data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetUserView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    renderer_classes = [JSONRenderer]
    serializer_class = GetUserSerializer

    @extend_schema(
        parameters=[
            OpenApiParameter("username", OpenApiTypes.STR, OpenApiParameter.QUERY),
            OpenApiParameter("email", OpenApiTypes.EMAIL, OpenApiParameter.QUERY),
        ]
    )
    def get(self, request, format=None, **params):

        user_not_found_message = {"message": "Requested user does not exist."}

        for query_param in ["username", "email"]:
            value = request.query_params.get(query_param)
            if value:
                try:
                    if query_param == "username":
                        user = User.objects.get(username=value)
                    elif query_param == "email":
                        user = User.objects.get(email=value)
                except User.DoesNotExist:
                    return Response(
                        user_not_found_message, status=status.HTTP_404_NOT_FOUND
                    )
                else:
                    serializer = GetUserSerializer(user)
                    return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class CreateUserView(APIView):
    permission_classes = [permissions.AllowAny]
    renderer_classes = [JSONRenderer]
    serializer_class = UserSerializer

    @extend_schema(
        responses={201: UserSerializer},
    )
    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            data_external_user = {
                "username": serializer.data["username"],
                "email": serializer.data["email"],
            }

            mail_subject = "Votre inscription sur Boukin"
            message = render_to_string(
                "confirm_registration.html",
                {
                    "username": serializer.data["username"],
                    "user_id": serializer.data["user_id"],
                },
            )
            # We have to select an SMTP service to send our email, will have to change
            from_email = "subscribe@boukin.org"
            try:
                send_mail(
                    mail_subject,
                    message=message,
                    from_email=from_email,
                    recipient_list=[f"{data_external_user['email']}"],
                    fail_silently=False,
                    html_message=message,
                )
            except SMTPException as error:
                error_message = f"Erreur lors de l'envoi de l'email d'activation de compte : {error}"
                return Response(
                    error_message, status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )

            return Response(data_external_user, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ActivateUserView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, **kwargs):
        user_id = kwargs["user_id"]
        if user_id:
            try:
                User.objects.get(user_id=user_id)
            except User.DoesNotExist as error:
                return Response(error, status=status.HTTP_404_NOT_FOUND)
            else:
                User.objects.filter(user_id=user_id).update(has_email_confirmed=True)
                return HttpResponseRedirect("https://boukin.adaschool.fr")
        return Response(status=status.HTTP_400_BAD_REQUEST)


class UpdateUserView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsRequestUser]
    renderer_classes = [JSONRenderer]
    serializer_class = UpdateUserSerializer

    def patch(self, request, format=None, **kwargs):
        allowed_field = {"email", "password", "username"}
        request_fields = set(request.data.keys())
        disallowed_fields = request_fields - allowed_field
        if disallowed_fields:
            error_message = "Vous ne pouvez modifier que l'email, le mot de passe ou le nom d'utilisateur·rice"
            return Response(
                {"message": error_message}, status=status.HTTP_400_BAD_REQUEST
            )

        try:
            user = User.objects.get(user_id=kwargs["user_id"])
        except User.DoesNotExist as error:
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        self.check_object_permissions(request, user)
        serializer = UpdateUserSerializer(user, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            resource_location = f'/users/{kwargs["user_id"]}/'
            headers = {"Content-Location": resource_location}
            return Response(headers=headers, status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CreateExternalUserView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    renderer_classes = [JSONRenderer]
    serializer_class = ExternalUserSerializer

    @extend_schema(
        responses={201: ExternalUserSerializer},
    )
    def post(self, request, format=None):
        serializer = ExternalUserSerializer(data=request.data)
        if serializer.is_valid():
            saved_external_user = serializer.save()
            data_user = {
                "username": saved_external_user.username,
                "role": saved_external_user.role.label,
                "message": "Utilisateur-ice externe créé-e. Amitié créée.",
            }

            Friendship.objects.create(
                requesting_user=request.user,
                addressed_user=saved_external_user,
                status=Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE,
            )
            return Response(data_user, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BooksView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    renderer_classes = [JSONRenderer]
    serializer_class = BookSerializer(many=True)

    @extend_schema(
        parameters=[
            OpenApiParameter(
                "isbn",
                OpenApiTypes.STR,
                OpenApiParameter.QUERY,
                description="ISBN used to search one book",
            ),
            OpenApiParameter(
                "title",
                OpenApiTypes.STR,
                OpenApiParameter.QUERY,
                description="Title used to search books only if ISBN has not been provided",
            ),
            OpenApiParameter(
                "author_lname",
                OpenApiTypes.STR,
                OpenApiParameter.QUERY,
                description="Author last name used to search books only if isbn or title has not been provided",
            ),
            OpenApiParameter(
                "author_fname",
                OpenApiTypes.STR,
                OpenApiParameter.QUERY,
                description="Author first name used to search books if author last name has been provided too",
                required=False,
            ),
        ]
    )
    def get(self, request, **params):
        for query_param in ["isbn", "title", "author_lname"]:
            value = request.query_params.get(query_param)
            if value and value.strip():
                value = value.strip()
                if query_param == "isbn":
                    books = Book.objects.filter(isbn=value)
                elif query_param == "title":
                    books = Book.objects.filter(title__icontains=value)
                elif query_param == "author_lname":
                    books = Book.objects.filter(author_lname__iexact=value)
                    author_fname = request.query_params.get("author_fname")
                    if author_fname:
                        books = books.filter(author_fname__iexact=author_fname)

                if not books:
                    book_not_found_message = {
                        "message": "Aucun livre ne correspond à la recherche."
                    }
                    return Response(
                        book_not_found_message, status=status.HTTP_404_NOT_FOUND
                    )
                else:
                    serializer = BookSerializer(books, many=True)
                    return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(
        responses={201: BookSerializer},
    )
    def post(self, request, format=None):
        serializer = BookSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OwnedBookView(APIView):
    authentication_classes = [TokenAuthentication]
    renderer_classes = [JSONRenderer]
    serializer_class = CreateOwnedBookSerializer

    def get_permissions(self, *args, **kwargs):
        if self.request.method == "GET":
            return [permissions.IsAuthenticated(), IsRequestUser()]
        if self.request.method == "POST":
            return [permissions.IsAuthenticated(), IsRequestUser()]

    @extend_schema(responses={200: ListOwnedBooksSerializer(many=True)})
    def get(self, request, format=None):
        try:
            owner = User.objects.get(user_id=request.user.user_id)
        except User.DoesNotExist as error:
            return Response(error, status=status.HTTP_404_NOT_FOUND)
        queryset = OwnedBook.objects.filter(owner=owner)
        serializer = ListOwnedBooksSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(responses={201: CreateOwnedBookSerializer})
    def post(self, request, format=None):
        try:
            owner = User.objects.get(user_id=request.data["owner"])
        except User.DoesNotExist as error:
            return Response(error, status=status.HTTP_404_NOT_FOUND)
        self.check_object_permissions(request, owner)
        serializer = CreateOwnedBookSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ModifyOwnedBookView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsBookOwnerOrReadOnly]
    renderer_classes = [JSONRenderer]
    serializer_class = UpdateOwnedBookStatusSerializer

    @extend_schema(
        responses={204: UpdateOwnedBookStatusSerializer},
    )
    def patch(self, request, format=None, **kwargs):
        allowed_field = {"status"}
        request_fields = set(request.data.keys())
        disallowed_fields = request_fields - allowed_field

        if disallowed_fields:
            error_message = "Vous ne pouvez modifier que le statut du livre"
            return Response(
                {"message": error_message}, status=status.HTTP_400_BAD_REQUEST
            )

        try:
            book = OwnedBook.objects.get(owned_book_id=kwargs["owned_book_id"])
        except OwnedBook.DoesNotExist as error:
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        self.check_object_permissions(request, book)

        serializer = UpdateOwnedBookStatusSerializer(
            book, data=request.data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            resource_location = f'/owned-books/{kwargs["owned_book_id"]}/'
            headers = {"Content-Location": resource_location}
            return Response(
                headers=headers,
                status=status.HTTP_204_NO_CONTENT,
            )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(
        responses={200: OpenApiTypes.STR},
    )
    def delete(self, request, format=None, **kwargs):
        try:
            book = OwnedBook.objects.get(owned_book_id=kwargs["owned_book_id"])
        except ValidationError:
            return Response(
                "Ce livre n'existe pas.", status=status.HTTP_400_BAD_REQUEST
            )

        self.check_object_permissions(request, book)

        if book.status == "loaned":
            return Response(
                "Vous ne pouvez supprimer un livre en cours de prêt.",
                status=status.HTTP_403_FORBIDDEN,
            )

        book.delete()
        return Response("Livre supprimé.", status=status.HTTP_200_OK)


class ListFriendOwnedBooksView(APIView):
    permission_classes = [IsAFriend]
    authentication_classes = [TokenAuthentication]
    renderer_classes = [JSONRenderer]
    serializer_class = ListOwnedBooksSerializer(many=True)

    def get(self, request, format=None, **kwargs):
        if not kwargs["user_id"]:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        try:
            owner = User.objects.get(user_id=kwargs["user_id"])
        except User.DoesNotExist as error:
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        self.check_object_permissions(request, owner)

        queryset = OwnedBook.objects.filter(
            owner=owner,
            status__in=[
                OwnedBook.OwnedBookStatus.AVAILABLE,
                OwnedBook.OwnedBookStatus.LOANED,
            ],
        )
        serializer = ListOwnedBooksSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class GetAllLoansView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    renderer_classes = [JSONRenderer]
    serializer_class = GetAllLoansSerializer

    def get(self, request, format=None, **kwargs):

        if not kwargs["user_id"]:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if request.user.user_id != kwargs["user_id"]:
            return Response(status=status.HTTP_403_FORBIDDEN)
        queryset = Loan.objects.filter(owner=kwargs["user_id"]) | Loan.objects.filter(
            borrower=kwargs["user_id"]
        )
        total_number_of_loans = queryset.count()
        if queryset:
            serializer = GetAllLoansSerializer(queryset, many=True)
            data = {
                "total_number_of_loans": total_number_of_loans,
                "loans": serializer.data,
            }
            return Response(data, status=status.HTTP_200_OK)
        else:
            error_message = "Vous n'avez pas de prêts en cours"
            return Response(
                {"error_message": error_message}, status=status.HTTP_404_NOT_FOUND
            )


class CreateLoanByBookOwnerView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsBookOwnerOrReadOnly]
    renderer_classes = [JSONRenderer]
    serializer_class = CreateLoanSerializer

    @extend_schema(responses={201: CreateLoanSerializer})
    def post(self, request, format=None):

        try:
            book = OwnedBook.objects.get(owned_book_id=request.data["owned_book"])
        except OwnedBook.DoesNotExist as error:
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        self.check_object_permissions(request, book)

        serializer = CreateLoanSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UpdateLoanView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsLoanOwnerOrBorrower]
    renderer_classes = [JSONRenderer]
    serializer_class = UpdateLoanSerializer

    @extend_schema(
        responses={204: UpdateLoanSerializer},
    )
    def patch(self, request, format=None, **kwargs):

        allowed_fields = {"status", "starting_date", "ending_date"}
        request_fields = set(request.data.keys())
        disallowed_fields = request_fields - allowed_fields

        if disallowed_fields:
            error_message = "Vous ne pouvez modifier que le statut du prêt"
            return Response(
                {"message": error_message}, status=status.HTTP_400_BAD_REQUEST
            )

        try:
            loan = Loan.objects.get(loan_id=kwargs["loan_id"])
        except Loan.DoesNotExist as error:
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        self.check_object_permissions(request, loan)
        serializer = UpdateLoanSerializer(
            loan, context={"user": request.user}, data=request.data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            resource_location = (
                f'/users/{request.user.user_id}/loans/{kwargs["loan_id"]}/'
            )
            headers = {"Content-Location": resource_location}
            return Response(
                headers=headers,
                status=status.HTTP_204_NO_CONTENT,
            )

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CreateFriendshipView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    renderer_classes = [JSONRenderer]
    serializer_class = FriendshipSerializer

    @extend_schema(responses={201: FriendshipSerializer})
    def post(self, request, format=None):
        serializer = FriendshipSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ModifyFriendshipView(APIView):

    serializer_class = UpdateFriendshipSerializer

    def get_permissions(self, *args, **kwargs):
        if self.request.method == "PATCH":
            return [
                permissions.IsAuthenticated(),
                IsFriendshipMember(),
                IsStatusUpdateOnly(),
            ]
        if self.request.method == "DELETE":
            return [permissions.IsAuthenticated(), IsRequestUser()]

    @extend_schema(
        responses={204: UpdateFriendshipSerializer},
    )
    def patch(self, request, format=None, **kwargs):
        try:
            friendship = Friendship.objects.get(friendship_id=kwargs["friendship_id"])
        except Friendship.DoesNotExist:
            return Response(
                "Vous n'avez pas de demande d'amitié en cours avec cette personne.",
                status=status.HTTP_404_NOT_FOUND,
            )
        self.check_object_permissions(request, friendship)
        serializer = UpdateFriendshipSerializer(
            friendship, data=request.data, partial=True, context={"user": request.user}
        )
        if serializer.is_valid():
            serializer.save()
            resource_location = f'/friendships/{kwargs["friendship_id"]}/'
            headers = {"Content-Location": resource_location}
            return Response(
                headers=headers,
                status=status.HTTP_204_NO_CONTENT,
            )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, format=None, **kwargs):
        try:
            friendship = Friendship.objects.get(friendship_id=kwargs["friendship_id"])
        except Friendship.DoesNotExist:
            return Response(
                "Vous n'avez pas de demande d'amitié en cours avec cette personne.",
                status=status.HTTP_404_NOT_FOUND,
            )
        self.check_object_permissions(request, friendship.requesting_user)
        if friendship.status != Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED:
            return Response(
                "Vous ne pouvez supprimer une amitié en cours.",
                status=status.HTTP_403_FORBIDDEN,
            )

        friendship.delete()
        return Response("Amitié supprimée.", status=status.HTTP_200_OK)


class RequestLoanByFriendView(APIView):
    permission_classes = [permissions.IsAuthenticated, IsAFriend]
    renderer_classes = [JSONRenderer]
    serializer_class = RequestLoanSerializer

    @extend_schema(
        responses={201: RequestLoanSerializer},
    )
    def post(self, request, format=None):

        try:
            book = OwnedBook.objects.get(owned_book_id=request.data["owned_book"])
        except OwnedBook.DoesNotExist as error:
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        self.check_object_permissions(request, book.owner)

        serializer = RequestLoanSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetUserFriendshipsView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    renderer_classes = [JSONRenderer]
    serializer_class = GetUserFriendshipsSerializer

    @extend_schema(operation_id="list_user_friendships")
    def get(self, request, format=None, **kwargs):

        if request.user.user_id != kwargs["user_id"]:
            return Response(status=status.HTTP_403_FORBIDDEN)

        friendships = Friendship.objects.filter(
            requesting_user=kwargs["user_id"]
        ) | Friendship.objects.filter(addressed_user=kwargs["user_id"])
        if friendships:
            serializer = GetUserFriendshipsSerializer(friendships, many=True)

            data = []

            for friendship in serializer.data:
                if (
                    str(request.user.user_id)
                    == friendship["requesting_user"]["user_id"]
                ):
                    friend = friendship["addressed_user"]
                    friend["is_requesting_user"] = False
                else:
                    friend = friendship["requesting_user"]
                    friend["is_requesting_user"] = True

                friendship_data = {
                    "friendship_id": friendship["friendship_id"],
                    "status": friendship["status"],
                    "friend": friend,
                }
                data.append(friendship_data)
            return Response(data, status=status.HTTP_200_OK)
        else:
            error_message = "Vous n'avez pas encore d'amitiés."
            return Response(
                {"error_message": error_message}, status=status.HTTP_404_NOT_FOUND
            )


class GetUserSingleFriendshipView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    renderer_classes = [JSONRenderer]
    serializer_class = GetUserFriendshipsSerializer

    @extend_schema(operation_id="retrieve_user_single_friendship")
    def get(self, request, format=None, **kwargs):

        try:
            friendship = Friendship.objects.get(friendship_id=kwargs["friendship_id"])

        except Friendship.DoesNotExist:
            error_message = "Cette amitié n'existe pas."
            return Response(
                {"error_message": error_message}, status=status.HTTP_404_NOT_FOUND
            )

        if (
            request.user != friendship.addressed_user
            and request.user != friendship.requesting_user
        ):
            return Response(status=status.HTTP_403_FORBIDDEN)

        serializer = GetUserFriendshipsSerializer(friendship)

        if str(request.user.user_id) == serializer.data["requesting_user"]["user_id"]:
            friend = serializer.data["addressed_user"]
            friend["is_requesting_user"] = False
        else:
            friend = serializer.data["requesting_user"]
            friend["is_requesting_user"] = True

        data = {
            "friendship_id": serializer.data["friendship_id"],
            "status": serializer.data["status"],
            "friend": friend,
        }

        return Response(data, status=status.HTTP_200_OK)
