"""
Creating models for database
"""

import uuid
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager


class UserManager(BaseUserManager):
    def create_user(self, email, username, has_email_confirmed, password):

        user = self.model(
            email=email,
            username=username,
            password=password,
            has_email_confirmed=has_email_confirmed,
        )
        user.is_superuser = False
        user.is_admin = False
        user.is_staff = False
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_superuser", True)
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self.create_user(email, password, **extra_fields)


class User(AbstractBaseUser):
    """
    user model
    """

    class UserRole(models.TextChoices):
        """
        user role class
        """

        USER = "user"
        ADMIN = "admin"
        OWNER = "owner"
        EXTERNAL = "external"

    objects = UserManager()
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username", "has_email_confirmed"]
    is_active = True
    has_email_confirmed = models.BooleanField(default=False)
    user_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = models.CharField(max_length=30)
    email = models.EmailField(unique=True, null=True)
    password = models.CharField(max_length=255)
    role = models.CharField(choices=UserRole, default=UserRole.USER)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)


class Book(models.Model):
    """
    book model
    """

    book_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=255)
    author_fname = models.CharField(max_length=30)
    author_lname = models.CharField(max_length=30)
    isbn = models.CharField(max_length=13, unique=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)


class OwnedBook(models.Model):
    """
    owned_book model
    """

    class OwnedBookStatus(models.TextChoices):
        """
        OwnedBook status class
        """

        AVAILABLE = "available"
        LOANED = "loaned"
        NOT_VISIBLE = "not visible"

    owned_book_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    owner = models.ForeignKey(
        "User", on_delete=models.CASCADE, related_name="owned_books"
    )
    book = models.ForeignKey("Book", on_delete=models.PROTECT)
    status = models.CharField(
        choices=OwnedBookStatus, default=OwnedBookStatus.NOT_VISIBLE
    )


class Loan(models.Model):
    """
    loan model
    """

    class LoanStatus(models.TextChoices):
        """
        Loan status class
        """

        LOAN_REQUESTED = "loan_requested"
        LOAN_DENIED = "loan_denied"
        LOAN_ACTIVE = "loan_active"
        RETURN_REQUESTED = "return_requested"
        RETURN_CONFIRMATION_PENDING = "return_confirmation_pending"
        RETURN_CONFIRMED = "return_confirmed"

    loan_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owner = models.ForeignKey(
        "User", on_delete=models.PROTECT, related_name="lended_books"
    )
    borrower = models.ForeignKey(
        "User", on_delete=models.PROTECT, related_name="borrowed_books"
    )
    owned_book = models.ForeignKey("OwnedBook", on_delete=models.CASCADE)
    status = models.CharField(choices=LoanStatus, default=LoanStatus.LOAN_REQUESTED)
    starting_date = models.DateField(blank=True, null=True)
    ending_date = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Friendship(models.Model):
    """
    friendship model
    """

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["requesting_user", "addressed_user"],
                name="unique_friendship_couple",
            ),
        ]

    class FriendshipStatus(models.TextChoices):
        """
        Friendship status class
        """

        FRIENDSHIP_REQUESTED = "friendship_requested"
        FRIENDSHIP_DENIED = "friendship_denied"
        FRIENDSHIP_ACTIVE = "friendship_active"
        FRIENDSHIP_OVER = "friendship_over"

    friendship_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    requesting_user = models.ForeignKey(
        "User", on_delete=models.PROTECT, related_name="addressed_friends"
    )
    addressed_user = models.ForeignKey(
        "User", on_delete=models.PROTECT, related_name="requesting_friends"
    )
    status = models.CharField(
        choices=FriendshipStatus, default=FriendshipStatus.FRIENDSHIP_REQUESTED
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
