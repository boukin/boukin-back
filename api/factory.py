import string
import secrets
from api.models import Book, Friendship, Loan, OwnedBook, User
from random import randint, choices


class BookFactory:
    def new(self):
        title_length = randint(10, 30)
        title = "".join(choices(string.ascii_letters, k=title_length))
        isbn = "978" + str(randint(1111111111, 9999999999))
        book = Book.objects.create(
            title=title, author_fname="John", author_lname="Doe", isbn=isbn
        )
        return book


class UserFactory:
    def new(self):
        name_length = randint(10, 30)
        username = "".join(choices(string.ascii_letters, k=name_length))
        email = username + "@boukin.org"
        password = secrets.token_urlsafe(20)
        user = User.objects.create(
            username=username, email=email, password=password, role=User.UserRole.USER
        )
        return user


class OwnedBookFactory:
    def new(
        self,
        book: Book,
        owner: User,
        status: OwnedBook.OwnedBookStatus = OwnedBook.OwnedBookStatus.NOT_VISIBLE,
    ):
        owned_book = OwnedBook.objects.create(owner=owner, book=book, status=status)
        return owned_book


class LoanFactory:
    def new(
        self,
        owner: User,
        borrower: User,
        owned_book: OwnedBook,
        status: Loan.LoanStatus = Loan.LoanStatus.LOAN_REQUESTED,
    ):
        loan = Loan.objects.create(
            owner=owner, borrower=borrower, owned_book=owned_book, status=status
        )
        return loan


class FriendshipFactory:
    def new(
        self,
        requesting_user: User,
        addressed_user: User,
        status: Friendship.FriendshipStatus = Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED,
    ):
        friendship = Friendship.objects.create(
            requesting_user=requesting_user,
            addressed_user=addressed_user,
            status=status,
        )
        return friendship
