from rest_framework import serializers
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
from django.utils.translation import gettext as _
from django.core.validators import RegexValidator
from rest_framework.exceptions import ValidationError
from .models import User, Book, OwnedBook, Loan, Friendship
from .rules import LoanRules


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "user_id",
            "username",
            "email",
            "password",
            "role",
            "created_at",
            "updated_at",
        ]

    def validate_password(self, value: str):
        #    At least one lowercase letter ((?=.*[a-z]))
        #    At least one uppercase letter ((?=.*[A-Z]))
        #    At least one digit ((?=.*\d))
        #    At least one special character among @$!%*?& ((?=.*[@$!%*?&]))
        #    Length between 8 and 20 ({8,20})
        password_regex = (
            r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$"
        )
        regex_validator = RegexValidator(
            password_regex, _("Le mot de passe n'est pas conforme")
        )
        regex_validator(value)
        hashed_password = make_password(value, salt=None, hasher="default")
        return hashed_password


class GetUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "user_id",
            "username",
        ]


class GetFriendUserSerializer(GetUserSerializer):
    class Meta(GetUserSerializer.Meta):
        fields = GetUserSerializer.Meta.fields + [
            "email",
            "role",
        ]


class UpdateUserSerializer(UserSerializer):
    class Meta:
        model = User
        fields = ["username", "email", "password"]


class ExternalUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["user_id", "username", "role", "created_at", "updated_at"]

    def validate(self, data):

        data["role"] = User.UserRole.EXTERNAL

        return data


class GenerateTokenSerializer(serializers.Serializer):
    email = serializers.CharField(label=_("Email"), write_only=True)
    password = serializers.CharField(
        label=_("Password"),
        style={"input_type": "password"},
        trim_whitespace=False,
        write_only=True,
    )
    token = serializers.CharField(label=_("Token"), read_only=True)

    def validate(self, attrs):
        email = attrs.get("email")
        password = attrs.get("password")

        if email and password:
            user = authenticate(
                request=self.context.get("request"), email=email, password=password
            )

            # The authenticate call simply returns None for is_active=False
            # users. (Assuming the default ModelBackend authentication
            # backend.)
            if not user:
                msg = _("Unable to log in with provided credentials.")
                raise serializers.ValidationError(msg, code="authorization")
            if user.has_email_confirmed is False:
                msg = _("Unable to log in with unconfirmed account.")
                raise serializers.ValidationError(msg, code="authorization")
        else:
            msg = _('Must include "email" and "password".')
            raise serializers.ValidationError(msg, code="authorization")

        attrs["user"] = user
        return attrs


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = [
            "book_id",
            "title",
            "author_fname",
            "author_lname",
            "isbn",
            "created_at",
            "updated_at",
        ]

    # TO IMPLEMENT: ISBN validation (10 digits without 978-, 13 digits with 978-),
    # raise ValidationError from Django if incorrect format is entered.
    # This function or another one should remove all hyphens ("-")
    # before writing data in table, to only get a string of integers.
    def validate_isbn(self, value: str):
        if len(value) != 10 and len(value) != 13:
            raise ValidationError(
                _("Un ISBN valide doit comporter 10 ou 13 caractères"),
                code="invalid_isbn",
            )
        return value


class OwnedBookSerializer(serializers.ModelSerializer):
    owner = GetUserSerializer(read_only=True)
    book = BookSerializer()

    class Meta:
        model = OwnedBook
        fields = ["owned_book_id", "owner", "book", "status"]


class CreateOwnedBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = OwnedBook
        fields = ["owned_book_id", "owner", "book", "status"]


class ListOwnedBooksSerializer(serializers.ModelSerializer):
    class Meta:
        model = OwnedBook
        fields = ["owned_book_id", "book", "status"]
        depth = 1


class UpdateOwnedBookStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = OwnedBook
        fields = ["status"]

    def validate_status(self, value):
        if self.instance.status == OwnedBook.OwnedBookStatus.LOANED:
            ongoing_status = [
                Loan.LoanStatus.LOAN_ACTIVE,
                Loan.LoanStatus.RETURN_REQUESTED,
                Loan.LoanStatus.RETURN_CONFIRMATION_PENDING,
            ]
            ongoing_loan = Loan.objects.filter(
                owned_book=self.instance, status__in=ongoing_status
            ).first()
            if ongoing_loan:
                raise ValidationError(
                    _(
                        "Un prêt est toujours en cours pour ce livre, vous ne pouvez pas changer son statut."
                    )
                )

        if value == OwnedBook.OwnedBookStatus.LOANED:
            raise ValidationError(
                _("Créez un prêt pour changer le statut de ce livre.")
            )

        return value


class LoanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Loan
        fields = [
            "loan_id",
            "owner",
            "borrower",
            "owned_book",
            "status",
            "starting_date",
            "ending_date",
            "created_at",
            "updated_at",
        ]

    def validate(self, data):
        if data["owned_book"].status == OwnedBook.OwnedBookStatus.LOANED:
            raise ValidationError(
                _("Vous ne pouvez pas emprunter un livre en cours de prêt.")
            )

        if data["owner"] == data["borrower"]:
            raise ValidationError(_("Vous ne pouvez pas vous prêter un livre."))

        if "ending_date" in data:
            if data["starting_date"] > data["ending_date"]:
                raise ValidationError(
                    _(
                        "La date de début d'emprunt ne peut être après celle de fin d'emprunt."
                    )
                )

        return data


class CreateLoanSerializer(LoanSerializer):
    class Meta(LoanSerializer.Meta):
        model = Loan

    def validate(self, data):

        data = super().validate(data)

        data["status"] = Loan.LoanStatus.LOAN_ACTIVE

        # Need to make a request to update the OwnedBook object in the database.
        OwnedBook.objects.filter(owned_book_id=data["owned_book"].owned_book_id).update(
            status=OwnedBook.OwnedBookStatus.LOANED
        )
        return data


class RequestLoanSerializer(LoanSerializer):
    class Meta(LoanSerializer.Meta):
        model = Loan

    def validate(self, data):

        data = super().validate(data)

        already_requested_loan = (
            data["borrower"]
            .borrowed_books.filter(
                owner=data["owner"],
                owned_book=data["owned_book"],
                status=Loan.LoanStatus.LOAN_REQUESTED,
            )
            .first()
        )

        if already_requested_loan:
            raise ValidationError(
                _("Vous avez déjà une demande d'emprunt en cours pour ce livre.")
            )

        data["status"] = Loan.LoanStatus.LOAN_REQUESTED

        return data


class UpdateLoanSerializer(LoanSerializer):
    class Meta(LoanSerializer.Meta):
        model = Loan
        fields = ["status", "starting_date", "ending_date"]

    def validate(self, data):
        loan = self.instance
        user = self.context["user"]
        loan_rules = LoanRules(loan, data)

        if user == loan.owner:

            validation_error_message = loan_rules.check_owner_rules()
            if validation_error_message:
                raise ValidationError(_(validation_error_message))

        if user == loan.borrower:
            validation_error_message = loan_rules.check_borrower_rules()
            if validation_error_message:
                raise ValidationError(_(validation_error_message))

        return data


class GetAllLoansSerializer(LoanSerializer):
    owner = GetUserSerializer(read_only=True)
    borrower = GetUserSerializer(read_only=True)
    owned_book = OwnedBookSerializer()

    class Meta(LoanSerializer.Meta):
        model = Loan
        fields = [
            "loan_id",
            "owner",
            "borrower",
            "owned_book",
            "status",
            "starting_date",
            "ending_date",
        ]


class FriendshipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Friendship
        fields = [
            "friendship_id",
            "requesting_user",
            "addressed_user",
            "status",
            "created_at",
            "updated_at",
        ]

    def validate(self, data):
        if data["requesting_user"] == data["addressed_user"]:
            raise ValidationError(_("Vous ne pouvez pas être ami-e avec vous-même."))

        try:
            already_existing_friendship = Friendship.objects.get(
                requesting_user=data["requesting_user"],
                addressed_user=data["addressed_user"],
            )
        except Exception:
            already_existing_friendship = Friendship.objects.filter(
                requesting_user=data["addressed_user"],
                addressed_user=data["requesting_user"],
            ).first()

        if already_existing_friendship:

            if (
                already_existing_friendship.status
                == Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED
            ):
                raise ValidationError(_("Une demande d'amitié est déjà en cours."))
            if (
                already_existing_friendship.status
                == Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE
            ):
                raise ValidationError(_("Vous êtes déjà ami-e avec cette personne."))
            if (
                already_existing_friendship.status
                == Friendship.FriendshipStatus.FRIENDSHIP_DENIED
            ):
                raise ValidationError(
                    _("Vous ne pouvez demander cette personne en ami-e.")
                )
            if (
                already_existing_friendship.status
                == Friendship.FriendshipStatus.FRIENDSHIP_OVER
            ):
                raise ValidationError(
                    _("Vous ne pouvez demander cette personne en ami-e.")
                )
        return data


class UpdateFriendshipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Friendship
        fields = ["status"]

    def validate(self, data):

        user = self.context["user"]
        friendship = self.instance

        if friendship.status == Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE:
            if data["status"] == Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED:
                raise ValidationError(
                    _(
                        "Il n'est pas possible de passer d'une amitié active à une demande d'amitié."
                    )
                )
            if data["status"] == Friendship.FriendshipStatus.FRIENDSHIP_DENIED:
                raise ValidationError(
                    _(
                        "Il n'est pas possible de passer d'une amitié active à refusée. Vous pouvez néanmoins mettre fin à cette amitié."
                    )
                )
            if data["status"] == Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE:
                raise ValidationError(
                    _("Votre amitié avec cette personne est déjà active.")
                )

        if user == friendship.requesting_user:

            if friendship.status == Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED:
                raise ValidationError(
                    _(
                        "Seul l'utilisateur qui a reçu la demande peut modifier le statut de cette amitié."
                    )
                )
            if friendship.status == Friendship.FriendshipStatus.FRIENDSHIP_DENIED:
                raise ValidationError(
                    _(
                        "Votre demande d'amitié a été refusée. Vous ne pouvez pas modifier le statut de cette amitié."
                    )
                )
            if (
                friendship.status == Friendship.FriendshipStatus.FRIENDSHIP_OVER
                and data["status"] != Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED
            ):
                raise ValidationError(
                    _(
                        "Votre amitié avec cette personne est terminée. Veuillez renvoyer une demande d'amitié."
                    )
                )

        if user == friendship.addressed_user:

            if friendship.status == Friendship.FriendshipStatus.FRIENDSHIP_OVER:
                raise ValidationError(
                    _(
                        "Votre amitié avec cette personne est terminée. Vous ne pouvez pas en modifier le statut."
                    )
                )
            if (
                friendship.status == Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED
                and data["status"] != Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE
                and data["status"] != Friendship.FriendshipStatus.FRIENDSHIP_DENIED
            ):
                raise ValidationError(
                    _("Vous ne pouvez qu'accepter ou refuser cette demande d'amitié.")
                )
            if (
                friendship.status == Friendship.FriendshipStatus.FRIENDSHIP_DENIED
                and data["status"] != Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED
            ):
                raise ValidationError(
                    _(
                        "Vous avez déjà refusé cette demande d'amitié. Veuillez en renvoyer une pour en modifier le statut."
                    )
                )
            if friendship.status == Friendship.FriendshipStatus.FRIENDSHIP_ACTIVE:
                if data["status"] == Friendship.FriendshipStatus.FRIENDSHIP_OVER:
                    friendship.addressed_user, friendship.requesting_user = (
                        friendship.requesting_user,
                        user,
                    )
            if friendship.status == Friendship.FriendshipStatus.FRIENDSHIP_DENIED:
                if data["status"] == Friendship.FriendshipStatus.FRIENDSHIP_REQUESTED:
                    friendship.addressed_user, friendship.requesting_user = (
                        friendship.requesting_user,
                        user,
                    )

        return data


class GetUserFriendshipsSerializer(serializers.ModelSerializer):
    requesting_user = GetFriendUserSerializer(read_only=True)
    addressed_user = GetFriendUserSerializer(read_only=True)

    class Meta(FriendshipSerializer.Meta):
        model = Friendship
        fields = ["friendship_id", "requesting_user", "addressed_user", "status"]
