from .models import Loan


class LoanRules:

    def __init__(self, loan, data):
        self.loan = loan
        self.data = data
        self._common_rules = self.__set_common_rules()
        self._owner_rules = self.__set_owner_rules()
        self._borrower_rules = self.__set_borrower_rules()

    def __check_key(self, key):
        try:
            value = self.data[key]
        except KeyError:
            return None
        else:
            return value

    @property
    def starting_date(self):
        return self.__check_key("starting_date")

    @property
    def ending_date(self):
        return self.__check_key("ending_date")

    @property
    def update_status_value(self):
        return self.__check_key("status")

    def __set_common_rules(self):

        self.return_confirmed_cannot_be_modified_rules = [
            self.loan.status == Loan.LoanStatus.RETURN_CONFIRMED
        ]

    def __set_owner_rules(self):
        self.loan_requested_must_become_active_or_denied_rules = [
            self.loan.status == Loan.LoanStatus.LOAN_REQUESTED,
            self.update_status_value != Loan.LoanStatus.LOAN_ACTIVE,
            self.update_status_value != Loan.LoanStatus.LOAN_DENIED,
        ]
        self.loan_active_must_become_return_requested_or_return_confirmed_rules = [
            self.loan.status == Loan.LoanStatus.LOAN_ACTIVE,
            self.update_status_value != Loan.LoanStatus.RETURN_REQUESTED,
            self.update_status_value != Loan.LoanStatus.RETURN_CONFIRMED,
        ]
        self.loan_denied_can_become_active_rules = [
            self.loan.status == Loan.LoanStatus.LOAN_DENIED,
            self.update_status_value != Loan.LoanStatus.LOAN_ACTIVE,
        ]
        self.return_requested_must_become_confirmed_rules = [
            self.loan.status == Loan.LoanStatus.RETURN_REQUESTED,
            self.update_status_value != Loan.LoanStatus.RETURN_CONFIRMED,
        ]
        self.return_confirmation_pending_must_become_confirmed_rules = [
            self.loan.status == Loan.LoanStatus.RETURN_CONFIRMATION_PENDING,
            self.update_status_value != Loan.LoanStatus.RETURN_CONFIRMED,
        ]
        self.loan_dates_cannot_be_modified_outside_of_loan_request_confirmation = [
            self.loan.status != Loan.LoanStatus.LOAN_REQUESTED,
            self.update_status_value != Loan.LoanStatus.LOAN_ACTIVE,
            any([self.starting_date is not None, self.ending_date is not None]),
        ]

    def __set_borrower_rules(self):

        self.loan_active_must_receive_return_confirmation_pending_rules = [
            self.loan.status == Loan.LoanStatus.LOAN_ACTIVE,
            self.update_status_value != Loan.LoanStatus.RETURN_CONFIRMATION_PENDING,
        ]
        self.loan_requested_cannot_be_modified_rules = [
            self.update_status_value is not None,
            self.loan.status == Loan.LoanStatus.LOAN_REQUESTED,
        ]

        self.loan_denied_cannot_be_modified_rules = [
            self.loan.status == Loan.LoanStatus.LOAN_DENIED
        ]
        self.return_requested_must_receive_return_confirmation_pending_rules = [
            self.loan.status == Loan.LoanStatus.RETURN_REQUESTED,
            self.update_status_value != Loan.LoanStatus.RETURN_CONFIRMATION_PENDING,
        ]
        self.return_confirmation_pending_cannot_be_modified_rules = [
            self.loan.status == Loan.LoanStatus.RETURN_CONFIRMATION_PENDING
        ]
        self.starting_and_ending_dates_cannot_be_modified_rules = [
            self.starting_date is not None,
            self.ending_date is not None,
        ]

    def check_owner_rules(self):

        validation_error_message = ""

        if all(self.loan_dates_cannot_be_modified_outside_of_loan_request_confirmation):
            validation_error_message += (
                "Vous ne pouvez modifier les dates du prêt si vous l'avez accepté.\n"
            )

        if all(self.loan_requested_must_become_active_or_denied_rules):
            validation_error_message += (
                "Vous devez accepter ou refuser cette demande de prêt.\n"
            )
        elif all(
            self.loan_active_must_become_return_requested_or_return_confirmed_rules
        ):
            validation_error_message += (
                "Un livre en cours de prêt ne peut être demandé en prêt.\n"
            )
        elif all(self.loan_denied_can_become_active_rules):
            validation_error_message += "Vous avez refusé de prêter ce livre, vous ne pouvez modifier le statut de prêt, sauf si vous voulez autoriser le prêt.\n"
        elif all(self.return_requested_must_become_confirmed_rules):
            validation_error_message += "Vous avez demandé le retour de ce livre, vous ne pouvez modifier le statut de prêt, sauf si vous voulez confirmer le retour.\n"
        elif all(self.return_confirmation_pending_must_become_confirmed_rules):
            validation_error_message += "Votre confirmation de retour de prêt a été demandée, vous ne pouvez modifier le statut de prêt, sauf si vous voulez confirmer le retour.\n"
        elif all(self.return_confirmed_cannot_be_modified_rules):
            validation_error_message += "Vous avez confirmé le retour de ce livre, vous ne pouvez modifier le statut de prêt.\n"

        return validation_error_message

    def check_borrower_rules(self):

        validation_error_message = ""

        if any(self.starting_and_ending_dates_cannot_be_modified_rules):
            validation_error_message += (
                "Vous ne pouvez modifier les dates de début ou de fin de prêt.\n"
            )

        if all(self.loan_denied_cannot_be_modified_rules):
            validation_error_message += (
                "Ce prêt vous a été refusé, vous ne pouvez le modifier.\n"
            )
        elif all(self.loan_active_must_receive_return_confirmation_pending_rules):
            validation_error_message += "Ce prêt est en cours, vous ne pouvez en modifier le statut, sauf si vous rendez le livre prêté.\n"
        elif all(self.loan_requested_cannot_be_modified_rules):
            validation_error_message += (
                "Vous avez demandé ce prêt, vous ne pouvez en modifier le statut.\n"
            )
        elif all(self.return_requested_must_receive_return_confirmation_pending_rules):
            validation_error_message += "Vous avez une demande de retour en cours, vous ne pouvez en modifier le statut, sauf si vous rendez le livre prêté.\n"
        elif all(self.return_confirmation_pending_cannot_be_modified_rules):
            validation_error_message += "Votre retour de prêt a bien été enregistré, il est en attente de confirmation. Vous ne pouvez en modifier le statut.\n"
        elif all(self.return_confirmed_cannot_be_modified_rules):
            validation_error_message += "Votre retour de prêt a bien été confirmé. Vous ne pouvez en modifier le statut.\n"

        return validation_error_message
