# Boukin Back

## Installer et exécuter le projet dans un contexte de développement

### Avec Docker (recommandé)

```
git clone git@gitlab.com:boukin/boukin-back.git
git clone git@gitlab.com:boukin/boukin-front.git
cd boukin-back
docker compose --profile dev up
```

### Sans Docker

Nécessite l'installation de PostgreSQL et de Python.

```
git clone git@gitlab.com:boukin/boukin-back.git
cd boukin-back
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python3 manage.py makemigrations &&
python3 manage.py migrate &&
python3 manage.py runserver 0.0.0.0:8000
```

Windows (PowerShell) :

```
git clone git@gitlab.com:boukin/boukin-back.git
cd boukin-back
python3 -m venv venv
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
.\venv\Scripts\Activate.ps1
pip install -r requirements.txt
python3 manage.py makemigrations &&
python3 manage.py migrate &&
python3 manage.py runserver 0.0.0.0:8000
```

Quelque soit la configuration initiale, installation du *linter* (**flake8**) et du *formatter* (**black**) (nécessite Python et **pip**) :
```
pip install pre-commit
pre-commit install
```

## Lancer les tests

### Avec Docker
```
docker exec -ti boukin-back-dev-1 python3 manage.py test api/tests
```

### Sans Docker
```
python manage.py test api/tests
```

## Coverage

### Avec Docker

Pour générer le rapport

```
docker exec -ti boukin-back-dev-1 coverage run --source=api manage.py test api/tests
```

Pour afficher les résultats
Dans la console :

```
docker exec -ti boukin-back-dev-1 coverage report -m
```

### Sans Docker (environnement virtuel Python activé)

Pour générer le rapport
```
coverage run --source=api manage.py test api/tests
```

Pour afficher les résultats
Dans la console :
```
coverage report -m
```

Dans le navigateur :
```
coverage html
```

## OpenAPI documentation
Pour générer le fichier schema.yaml :

### Avec Docker
```
docker exec -ti boukin-back-dev-1 python3 manage.py spectacular --color --file schema.yml
```

### Sans Docker
```
python manage.py spectacular --color --file schema.yml
```
Pour lancer le container swagger et pouvoir consulter la documentation dans le navigateur :
(ne pas oublier de lancer Docker Desktop avant sous Windows et de lancer la commande suivante via PowerShell)

```
docker run -p 80:8080 -e SWAGGER_JSON=/schema.yml -v ${PWD}/schema.yml:/schema.yml swaggerapi/swagger-ui
```
