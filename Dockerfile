# syntax=docker/dockerfile:1

FROM python:3.12.7-slim-bullseye

ARG PORT=8000
ARG DBUSER=boukin
ARG DBNAME=boukin_db
ARG NUXTPORT=3001

WORKDIR /boukin-back

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY . .

WORKDIR /boukin-back/core/

RUN --mount=type=secret,id=db_password,env=DBPASSWORD \
    --mount=type=secret,id=secret_key,env=SECRET_KEY \
    --mount=type=secret,id=email_host_password,env=EMAIL_HOST_PASSWORD \
    printf "DBUSER=%s\nDBNAME=%s\nDBPASSWORD=%s\nSECRET_KEY=%s\nHOST=postgres\nDBPORT=5432\nNUXTPORT=%s\nEMAIL_HOST_PASSWORD=%s\n" $DBUSER $DBNAME $DBPASSWORD $SECRET_KEY $NUXTPORT $EMAIL_HOST_PASSWORD >> .env

WORKDIR /boukin-back

EXPOSE $PORT

CMD ["python3", "manage.py", "runserver", "0.0.0.0:$PORT"]
